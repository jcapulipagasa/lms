<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/check-login', 'LoginController@login');
Route::get('/check-auth', 'LoginController@checkAuth');
Route::post('/register-user', 'UserController@register');
Route::post('/reset-request', 'PasswordResetController@passwordResetRequest');
Route::post('/check-reset-request', 'PasswordResetController@checkResetRequest');
Route::post('/change-password', 'PasswordResetController@changePassword');

Route::get('/registration-notification', 'PageController@registrationConfirmation');

Route::middleware(['api'])->group(function () {
    
    Route::get('/dashboard', 'HomeController@index')->name('home');
    Route::get('/getName', 'LoginController@getName');

    Route::resource('bulletin', 'BulletinController');

    Route::resource('lecture', 'LectureController');
    Route::post('/edit-lecture/{id}', 'LectureController@update');
    Route::get('/download-lecture/{id}', 'LectureController@download');

    Route::resource('assignment', 'AssignmentController');
    Route::get('/assignment-admin-list/{id}', 'AssignmentController@indexAdmin');
    Route::post('/edit-assignment/{id}', 'AssignmentController@update');
    Route::get('/download-assignment/{id}', 'AssignmentController@download');
    Route::post('/upload-assignment-review/{id}/{aid}', 'AssignmentController@uploadReview');
    Route::get('/download-assignment-review/{id}', 'AssignmentController@downloadReview');

    Route::resource('assignment-list-data', 'AssignmentListController');
    Route::get('/assignment-titles', 'AssignmentListController@getTitles');

    Route::get('/approve-email-data', 'ApproveRequestController@index');
    Route::post('/approve-email/{id}', 'ApproveRequestController@approveRequest');

    Route::resource('about', 'AboutContentController');
    Route::get('/get-order', 'AboutContentController@getOrder');
    Route::get('/get-about-content', 'AboutContentController@indexStudent');

    Route::get('/getRole', 'HomeController@getRole');
    Route::get('/getUser', 'ChatController@getCurrentUser');
    Route::get('/contacts', 'ChatController@get');
    Route::get('/conversation/{id}', 'ChatController@getMessagesFor');
    Route::post('/conversation/send', 'ChatController@send');

    Route::get('/course', 'CourseController@index');
    Route::post('/save-course', 'CourseController@save');
    Route::get('/get-edit-course/{id}', 'CourseController@show');
    Route::post('/update-course/{id}', 'CourseController@update');
    Route::get('/delete-course/{id}', 'CourseController@delete');
    Route::get('/course-list', 'CourseController@courseList');

    Route::get('/module', 'ModuleController@index');
    Route::post('/save-module', 'ModuleController@save');
    Route::get('/get-edit-module/{id}', 'ModuleController@show');
    Route::post('/update-module/{id}', 'ModuleController@update');
    Route::post('/update-module-list', 'ModuleController@updateList');
    Route::get('/delete-module/{id}', 'ModuleController@delete');
    Route::get('/module-list/{id}', 'ModuleController@moduleList');
    Route::get('/module-list-title/{id}', 'ModuleController@getTitle');

    Route::get('/upload-module-index', 'UploadModuleController@index');
    Route::post('/update-upload-list', 'UploadModuleController@updateList');
    Route::post('/add-upload-module', 'UploadModuleController@save');
    Route::get('/get-update-upload-module/{id}', 'UploadModuleController@show');
    Route::post('/edit-upload-module/{id}', 'UploadModuleController@update');
    Route::get('/delete-upload-module/{id}', 'UploadModuleController@delete');
    Route::get('/download-resource/{id}/{idx}', 'UploadModuleController@download');
    Route::get('/update-file-download/{id}/{idx}', 'UploadModuleController@updateFileDownload');

    Route::get('/quiz-index/{id}', 'QuizController@index');
    Route::post('/add-quiz', 'QuizController@save');
    Route::get('/get-quiz/{id}', 'QuizController@show');
    Route::post('/update-quiz/{id}', 'QuizController@update');
    Route::get('/delete-quiz/{id}', 'QuizController@delete');

    Route::get('/get-course-display/{id}', 'StudentProgressController@courseDisplay');
    Route::get('/get-course-modules/{id}', 'StudentProgressController@moduleDisplay');
    Route::get('/get-course-module-upload/{id}', 'StudentProgressController@moduleUploadDisplay');
    Route::get('/get-quiz-items/{id}', 'StudentProgressController@getQuiz');
    Route::post('/check-quiz-answer', 'StudentProgressController@checkAnswer');
    Route::post('/update-module-progress', 'StudentProgressController@updateModuleProgress');
    Route::get('/check-student-quiz/{cid}/{mid}', 'StudentProgressController@checkQuiz');

    Route::get('/student-final-list-data', 'StudentFinalExamController@index');
    Route::get('/student-final-get-details/{id}', 'StudentFinalExamController@getDetails');
    Route::get('/check-if-final-passed/{id}', 'StudentFinalExamController@checkIfPassed');
    Route::get('/get-final-exam-items/{id}', 'StudentFinalExamController@getExam');
    Route::post('/compute-final-exam', 'StudentFinalExamController@getResult');
    
    Route::get('/final-exam-list-data', 'FinalExamController@index');
    Route::post('/save-final-exam', 'FinalExamController@save');
    Route::post('/update-final-exam-list', 'FinalExamController@updateList');
    Route::get('/get-edit-final-exam/{id}', 'FinalExamController@show');
    Route::post('/update-final-exam/{id}', 'FinalExamController@update');
    Route::get('/delete-final-exam/{id}/{cid}', 'FinalExamController@delete');

    Route::get('/final-exam-questions-data/{id}', 'FinalExamQuestionController@index');
    Route::post('/save-final-exam-question', 'FinalExamQuestionController@save');
    Route::get('/get-edit-final-exam-question/{id}', 'FinalExamQuestionController@show');
    Route::post('/update-final-exam-question/{id}', 'FinalExamQuestionController@update');
    Route::get('/delete-final-exam-question/{id}', 'FinalExamQuestionController@delete');

    Route::get('/view-student-grade-list', 'ViewGradesController@index');
    Route::get('/final-exam-student-list', 'ViewGradesController@finalExamStudentList');
    Route::post('/update-final-exam-student-list', 'ViewGradesController@updateFinalExamStudentList');
    Route::get('/display-grades/{id}', 'ViewGradesController@displayGrades');
    Route::post('/display-grades-filter', 'ViewGradesController@dispalyGradesFilter');
    Route::get('/display-grades-student', 'ViewGradesController@displayGradesStudent');
    Route::post('/display-grades-student-filter', 'ViewGradesController@displayGradesStudentFilter');

    Route::get('/course-certificate', 'ManageCourseCertificateController@index');
    // Route::post('/update-upload-list', 'UploadModuleController@updateList');
    Route::post('/add-course-certificate', 'ManageCourseCertificateController@save');
    Route::get('/get-course-certificate/{id}', 'ManageCourseCertificateController@show');
    Route::post('/edit-course-certificate/{id}', 'ManageCourseCertificateController@update');
    Route::get('/delete-course-certificate/{id}', 'ManageCourseCertificateController@delete');

    Route::get('/student-get-laboratory-manual', 'LaboratoryManualController@studentIndex');

    Route::get('/user-list-data', 'UserController@index');
    Route::post('/user-create', 'UserController@create');
    Route::get('/get-user-detail/{id}', 'UserController@show');
    Route::post('/update-user-details/{id}', 'UserController@update');
    Route::get('/disable-user/{id}', 'UserController@disableUser');
    Route::get('/enable-user/{id}', 'UserController@enableUser');

    Route::get('/get-student-enroll-list', 'StudentEnrollCourseController@index');
    Route::post('/save-student-enroll-course', 'StudentEnrollCourseController@save');
    Route::get('/get-student-enroll-course/{id}', 'StudentEnrollCourseController@getEdit');
    Route::post('/update-student-enroll-course', 'StudentEnrollCourseController@update');
    Route::get('/get-student-enroll-course', 'StudentEnrollCourseController@studentCourseList');

    Route::get('/get-final-exam-module/{id}', 'FinalExamModuleController@index');
    Route::get('/get-total-randon-exam/{id}', 'FinalExamModuleController@totalRandom');
    Route::post('/save-randon-exam-module', 'FinalExamModuleController@saveRandom');

    Route::get('/send-page-hits/{id}', 'FinalExamLogController@pageHit');

    Route::get('/student-course-filter-list', 'ViewGradesController@studentCourseList');
    Route::get('/admin-course-filter-list/{id}', 'ViewGradesController@adminCourseList');

    // Route::get('/format-scores', 'ViewGradesController@formatGrades');

    Route::get('/logout', 'LoginController@logout');

    // Route::get('/manual-pdf', 'StudentProgressController@manualCertificate');
    // Route::get('/manual-pdf-completion', 'StudentFinalExamController@manualCertificate');
    Route::get('/final-certificate', 'FinalCertificateController@manualCertificate');
    // Route::get('/get-serial', 'FinalCertificateController@getSerial');

});

Route::get('/{any}', 'HomeController@index')->where('any', '.*');