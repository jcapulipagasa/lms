<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'email' => 'bobongmd@yahoo.com',
            'email_verified_at' => date('Y-m-d H:i:s'),
            'password' => bcrypt('password'),
            'is_active' => 'Y',
        ]);

        DB::table('profiles')->insert([
            'user_id' => 1,
            'first_name' => 'Pedrito',
            'middle_name' => 'Y',
            'last_name' => 'Tagayuna',
            'role' => 'admin',
            'contact_number' => '',
            'online_status' => 'N',
        ]);
    }
}
