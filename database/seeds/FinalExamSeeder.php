<?php

use Illuminate\Database\Seeder;

class FinalExamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('final_exam_questions')->insert([
            [
                'final_exam_id' => '1',
                'question' => "Question for 1",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 2",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 3",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 4",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 5",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 6",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 7",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 8",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 9",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 10",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 11",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 12",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 13",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 14",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 15",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 16",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 17",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 18",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 19",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 20",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 21",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 22",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 23",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 24",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 25",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 26",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 27",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 28",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 29",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 30",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 31",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 32",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 33",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 34",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 35",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 36",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 37",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 38",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 39",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 40",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 41",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 42",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 43",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 44",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 45",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 46",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 47",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 48",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 49",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'final_exam_id' => '1',
                'question' => "Question for 50",
                'answer' => "Answer;Wrong1;Wrong2;Wrong3",
                'correct_answer' => 'Answer',
                'created_at' => date('Y-m-d H:i:s'),
            ]

        ]);
    }
}
