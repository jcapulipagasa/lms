<?php

use Illuminate\Database\Seeder;

class StudentFakerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        [
            'email' => 'beavis12345123@gmail.com',
            'email_verified_at' => date('Y-m-d H:i:s'),
            'password' => bcrypt('password'),
            'is_active' => 'Y',
        ],[
            'email' => 'jcapulipagasa@gmail.com',
            'email_verified_at' => date('Y-m-d H:i:s'),
            'password' => bcrypt('password'),
            'is_active' => 'N',
        ]
    ]);

        DB::table('profiles')->insert([
        [
            'user_id' => 2,
            'first_name' => 'John Christopher',
            'middle_name' => 'Galang',
            'last_name' => 'Apuli',
            'role' => 'faculty',
            'contact_number' => '',
            'online_status' => 'N',
        ],[
            'user_id' => 3,
            'first_name' => 'JC',
            'middle_name' => 'Galang',
            'last_name' => 'Apuli',
            'role' => 'user',
            'contact_number' => '',
            'online_status' => 'N',
        ]
        ]);
    }
}
