<?php

namespace App\Http\Helpers;

class ReformatFilename
{
    public static function reformat($input){

        $filename_arr = explode(".", $input);   // Splits and get the file
        $filename_type = end($filename_arr);    // extension of the filename

        $filename = str_replace(".$filename_type", "", $input);     //Remove file extension from filename
        $filename = preg_replace('!\s+!', ' ', $filename);          //Change 2 or more spaces into a single space
        $filename = str_replace(" ", "_", $filename);               //Replace all spaces with undescore
        $filename = str_replace(";", ".", $filename);               //Replace all semicolon with dot

        //This code prevents uploading of malicious file and convert it to unusable format 

        switch ($filename_type) {
            case 'doc':
                return $filename . ".doc";
                break;

            case 'docx':
                return $filename . ".docx";
                break;

            case 'pdf':
                return $filename . ".pdf";
                break;

            case 'xls':
                return $filename . ".xls";
                break;

            case 'xlsx':
                return $filename . ".xlsx";
                break;

            case 'ppt':
                return $filename . ".ppt";
                break;

            case 'pptx':
                return $filename . ".pptx";
                break;

            default:
                return $filename . ".usf";
                break;
        }

    }
}