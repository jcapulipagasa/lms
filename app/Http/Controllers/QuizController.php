<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quiz;

class QuizController extends Controller
{

    public function index($id){

        $quiz = Quiz::where('module_id', $id)
                ->get();

        return $quiz;

    }

    public function save(Request $request){

        $data = $request->except('_token');

        $arr_data = array(
            'module_id'         => $data['module_id'],
            'question'          => $data['question'],
            'correct_answer'    => $data['correct_answer']
        );

        $ans = '';

        foreach($data['answer'] as $answer){

            if($ans){
                $ans = $ans . ";" . $answer;
            } else{
                $ans = $answer;
            }

        }
        
        $arr_data['answer'] = $ans;

        $status = Quiz::create($arr_data);

        return response()->json($status);

    }

    public function show($id){

        $quiz = Quiz::where('id', $id)
                ->first();
                
        $answers = explode(';', $quiz['answer']);

        $quiz['answer'] = $answers;


        return $quiz;

    }

    public function update(Request $request, $id){

        $data = $request->except('_token');

        $arr_data = array(
            'question'          => $data['question'],
            'correct_answer'    => $data['correct_answer']
        );

        $ans = '';

        foreach($data['answer'] as $answer){

            if($ans){
                $ans = $ans . ";" . $answer;
            } else{
                $ans = $answer;
            }

        }
        
        $arr_data['answer'] = $ans;

        $status = Quiz::find($id)->update($arr_data);

        return response()->json($status);

    }

    public function delete($id){

        $status = Quiz::destroy($id);

        if($status){
            return response()->json($status);
        }

        return response()->json($status);        
    }

}
