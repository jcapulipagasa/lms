<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class PageController extends Controller
{
    public function index()
    {
        return view('lmsapp');
    }

    public function registrationConfirmation(){
        return view('auth.notification');
    }
}
