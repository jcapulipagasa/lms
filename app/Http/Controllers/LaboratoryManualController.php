<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LaboratoryManual;

use Auth;

class LaboratoryManualController extends Controller
{
    public function studentIndex(){

        $data = LaboratoryManual::where('user_id', Auth::user()->id)
                                    ->first();

        return $data;

    }
}
