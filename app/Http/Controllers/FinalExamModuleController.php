<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Module;
use App\FinalExamQuestion;

class FinalExamModuleController extends Controller
{
    public function index($id){

        $total_question = 0;

        $data = Module::withCount('final_exam')
                        ->where('course_id', $id)
                        ->get();

        foreach($data as $key => $val){
            $total_question = $total_question + $val['final_exam_count'];
        }

        return compact('data', 'total_question');

    }

    public function totalRandom($id){

        $data = Module::where('course_id', $id)
                        ->sum('randomized');
        
        return $data;
    }

    public function saveRandom(Request $request){

        $status = Module::where('id', $request['module_id'])
                ->update(array('randomized' => $request['randomized']));
        
        return $status;
    }
  

}
