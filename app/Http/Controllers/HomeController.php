<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        // $user    = User::find(Auth::user()->id);
        // $profile = $user->profile;

        // return view('home', compact('profile'));

        return view('lmsapp');

    }

    public function getRole(){

        $user = User::with('profile')->where('email', Auth::user()->email)->first()->toArray();
        return $user['profile']['role'];

    }
}
