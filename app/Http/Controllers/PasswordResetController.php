<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PasswordReset;
use App\User;
use Mail;
use Hash;

class PasswordResetController extends Controller
{
    public function passwordResetRequest(Request $request){

        $data = $request->except('_token');

        $email = filter_var($data['email'], FILTER_SANITIZE_EMAIL);

        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {

            $exist = User::where('email', $data['email'])   //Check whether email address is in the users table
                            ->exists();

            if($exist){

                $token = self::generateToken();

                $user = User::with('profile')
                            ->where('email', $data['email'])
                            ->first();

                if($user['email_verified_at'] == null || $user['is_active'] == 'N'){
                    return response()->json([
                        'message' => "User has not been verified or deactivated.",
                        'status' => 0
                    ], 200);
                }

                $hasEmail = PasswordReset::find($data['email']);
                
                if($hasEmail){

                    $hasEmail->token = $token;
                    $hasEmail->created_at = date('Y-m-d H:i:s');
                    $hasEmail->save();

                } else{

                    PasswordReset::create([
                        'email' => $data['email'],
                        'token' => $token,
                        'created_at' => date('Y-m-d H:i:s')
                    ]);

                }

                $user['token'] = $token;

                $mailed = self::mailToUser($user);

                if($mailed){

                    return response()->json([
                        'message' => "Password reset link has been succesfully sent to your email.",
                        'status' => 1
                    ], 200);

                } else{

                    return response()->json([
                        'message' => "Error sending reset link",
                        'status' => 0
                    ], 200);                    

                }

            } else{

                return response()->json([
                    'message' => "User is not registered",
                    'status' => 0
                ], 200);

            }

        } else{

            return response()->json([
                'message' => "Invalid email address format",
                'status' => 0
            ], 200);

        }

    }

    public function checkResetRequest(Request $request){
        
        $data = $request->only('check');

        $exist = PasswordReset::where('token', $data['check'])   //Check whether email address is in the users table
                    ->exists();

        if($exist){
            return response()->json([
                'message' => "Enter new password. Both fields are required.",
                'status' => 1
            ], 200);
        }

        return response()->json([
            'message' => "Link has been expired.",
            'status' => 0
        ], 200);

    }

    public function changePassword(Request $request){

        $data = $request->only('code', 'password');

        $email = PasswordReset::select('email')
                                ->where('token', $data['code'])
                                ->first();

        $user = User::where('email', $email['email'])
                    ->first();

        $user->password = Hash::make($data['password']);
        $status = $user->save();

        if($status){
            return response()->json([
                'message' => "Password changed successfully.",
                'status' => 1
            ], 200);
        }

        return response()->json([
            'message' => "Something went wrong. Please try again later.",
            'status' => 0
        ], 200);

    }

    private function generateToken(){

        $token = openssl_random_pseudo_bytes(16);
 
        //Convert the binary data into hexadecimal representation.
        $token = bin2hex($token);
        
        //Print it out for example purposes.
        return $token;

    }

    private function mailToUser($data){

        $to_name    = $data->profile['last_name'] . ", " .  $data->profile['first_name'] . " " . $data->profile['middle_name'];
        $to_email   = $data->email;
        $link       = $data->token;

        $data = array(
            'name' => $data->profile['first_name'],
            'link' => $link
        );

        Mail::send('email.reset', $data, function($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name)
                    ->subject('Password Reset Request');
            $message->from( config('mail.from.address') , 'admin@lms.app');
        });

        if (Mail::failures()) {
            return false;
        }else{
            return true;
        }

    }
}
