<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AssignmentList;
use App\Http\Helpers\ReformatFilename;
use Storage;
use App\User;
use File;
 
class AssignmentListController extends Controller
{

    private $sep = DIRECTORY_SEPARATOR;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $assignment_list = AssignmentList::all();
        return $assignment_list;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only('title', 'active');

        $id = AssignmentList::create($data)->id;

        $folder_name = storage_path("app" . $this->sep . "assignments" . $this->sep . "$id");

        $status = File::makeDirectory($folder_name, 0775, true);

        if($status){
            return response()->json($status);
        }

        return response()->json($status);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = AssignmentList::where('id', $id)->first()->toArray();

        if($result['active'] === 1){
            $result['active'] = true;
        } else{
            $result['active'] = false;
        }

        return $result;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $data = $request->only('title', 'active');

        $status = AssignmentList::find($id)->update($data);

        return response()->json($status);
 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    public function getTitles(){

        $assignment_list = [];

        $data = AssignmentList::where('active', 1)->get();

        foreach($data as $key => $value){
            $assignment_list[] = array(
                'text' => $value['title'],
                'value' => $value['id'],
            );
        }

        return $assignment_list;
    }

}
