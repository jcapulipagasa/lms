<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bulletin;


class BulletinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $bulletin = Bulletin::orderBy('schedule', 'desc')->get();

        foreach($bulletin as $key => $value){
            $bulletin[$key]['schedule'] = date('m/d/Y', strtotime($value['schedule']));
        }

        return $bulletin;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only('title', 'remarks', 'schedule');
        $timestamp = strtotime($data['schedule']);
        $day = date('D', $timestamp);

        $data['day'] = $day;

        $status = Bulletin::create($data);

        if($status){
            return response()->json($status);
        }

        return response()->json($status);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = Bulletin::where('id', $id)->first();
        return $result;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->only('title', 'remarks', 'schedule');
        $timestamp = strtotime($data['schedule']);
        $day = date('D', $timestamp);

        $data['day'] = $day;

        $status = Bulletin::find($id)->update($data);

        // if($status){
        //     return response()->json($status);
        // }

        return response()->json($status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $status = Bulletin::destroy($id);

        if($status){
            return response()->json($status);
        }

        return response()->json($status);        
    }

}
