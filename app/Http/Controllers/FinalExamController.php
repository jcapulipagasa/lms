<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FinalExam;
use App\Module;
use App\FinalExamQuestion;

class FinalExamController extends Controller
{
    public function index(){

        $final_exam = FinalExam::with('course')->get();
        return $final_exam;

    }

    public function save(Request $request){

        $data = $request->except('_token');
        $status = FinalExam::create($data);

        return $status;

    }

    public function show($id){

        $final_exam = FinalExam::where('id', $id)
                ->first();

        return $final_exam;

    }

    public function update(Request $request, $id){

        $data = $request->except('_token');

        $status = FinalExam::find($id)->update($data);

        return response()->json($status);

    }

    public function delete($id, $cid){

        $module_ids = [];

        $module = Module::select('id')
                        ->where('course_id', $cid)
                        ->get();

        foreach($module as $val){
            $module_ids[] = $val['id'];
        }

        Module::whereIn('id', $module_ids)->update(array('randomized' => 0));
        FinalExamQuestion::whereIn('module_id', $module_ids)->delete();

        $status = FinalExam::find($id)->delete();

        // DB::table('users')->whereIn('id', $array_of_ids)->update(array('votes' => 1));

        // FinalExamQuestion::where('final_exam_id', $id)->delete();
        // 

        return response()->json($status);

    }

    public function updateList(Request $request){

        $data = $request->except('_token');

        $final_exam = FinalExam::with('course')
                                ->where('course_id', $data['course_id'])
                                ->get();

        return $final_exam;

    } 


}
