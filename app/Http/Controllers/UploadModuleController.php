<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Module;
use App\UploadModule;
use App\Http\Helpers\ReformatFilename;
use App\FileDownloadStatus;
use Storage;
use Auth;

class UploadModuleController extends Controller
{

    private $sep = DIRECTORY_SEPARATOR;

    public function index(){

        $table = Module::with('course')
                ->with('upload')
                ->get();

        return $table;

    }


    public function updateList(Request $request){

        $data = $request->except('_token');

        if($data['course_id'] && $data['module_id']){
            $table = Module::with('course')
                    ->with('upload')
                    ->where('course_id', $data['course_id'])
                    ->where('id', $data['module_id'])
                    ->get();

            return $table;
        }

        $table = Module::with('course')
                ->with('upload')
                ->where('course_id', $data['course_id'])
                ->get();

        return $table;

    }

    public function save(Request $request){

        $data = $request->except('_token');

        $data_array = array(
            "module_id"         => $data['module_id'],
            "module_text"       => $data['module_text'],
            "external_link"     => $data['external_link'],
            // "path_to_vid"       => str_replace("watch?v=", "embed/", $data['path_to_vid']),
            "path_to_vid"       => $data['path_to_vid'],
            "path_to_webinar"   => $data['path_to_webinar'],
            // "webinar"           => $data['webinar'],
        );

        $id = UploadModule::create($data_array)->id;

        $folder_name = "online_courses" . $this->sep . $data['course_id'] . $this->sep . $data['module_id'];

        $files = $request->file('file');
        $path_str = '';

        if($files){

            foreach($files as $file){

                $name = ReformatFilename::reformat($file->getClientOriginalName());
    
                if($path_str){
                    $path_str = $path_str . ";" . $name;
                } else{
                    $path_str = $name;
                }
    
                $path_name = $folder_name . $this->sep . $name;
                $file->storeAs($folder_name, $name);
    
            }

            $doc_path['path_to_doc'] = $path_str;


        } else{
            $doc_path['path_to_doc'] = "";
        }

        $status = UploadModule::find($id)->update($doc_path);
        
        if($status){
            return response()->json('OK');
        }

        return response()->json("Error");

    }

    public function show($id)
    {
        $result = UploadModule::where('id', $id)->first()->toArray();

        $filenames = explode(";", $result['path_to_doc']);
        $result['path_to_vid'] = explode(",", $result['path_to_vid']);
        $result['external_link'] = explode(",", $result['external_link']);

        foreach($filenames as $file){
            $result['names'][] = $file;
        }

        return $result;
    }

    public function update(Request $request, $id)
    {

        $data = $request->except('_token');
        
        if($request->file){

            $data_array = array(
                "module_text"       => $data['module_text'],
                "external_link"     => $data['external_link'],
                // "path_to_vid"       => str_replace("watch?v=", "embed/", $path_to_vid),
                "path_to_vid"       => $data['path_to_vid'],
                "path_to_webinar"   => $data['path_to_webinar'],
                "webinar"           => $data['webinar'],
            );

        // if($request['file']){

            $result = UploadModule::where('id', $id)->first();
            $course = Module::with('course')
                        ->where('id', $result['module_id'])
                        ->first();

            $upload_module  = UploadModule::where('id', $id)->first();

            $folder_name    = "online_courses" . $this->sep . $course['course_id'] . $this->sep . $result['module_id'];

            Storage::deleteDirectory($folder_name);

            $studentFileStatus =  FileDownloadStatus::where('module_id', $result['module_id'])->delete();

            // $files = $request->file('file');

            $files = $request['file'];

            $path_str = '';

            foreach($files as $file){

                $name = ReformatFilename::reformat($file->getClientOriginalName());
    
                if($path_str){
                    $path_str = $path_str . ";" . $name;
                } else{
                    $path_str = $name;
                }
    
                $path_name = $folder_name . $this->sep . $name;
                $file->storeAs($folder_name, $name);
    
            }
    
            $data_array['path_to_doc'] = $path_str;

        } else{

            $path_to_vid = '';

            if($data['path_to_vid']){
    
                foreach($data['path_to_vid'] as $val){
    
                    if($path_to_vid){
                        $path_to_vid = $path_to_vid . "," . $val;
                    } else{
                        $path_to_vid = $val;
                    }
                }
            }
    
            $external_link = '';
    
            if($data['external_link']){
                foreach($data['external_link'] as $val){
    
                    if($external_link){
                        $external_link = $external_link . "," . $val;
                    } else{
                        $external_link = $val;
                    }
                }
            }
    
            $data_array = array(
                "module_text"       => $data['module_text'],
                "external_link"     => $external_link,
                // "path_to_vid"       => str_replace("watch?v=", "embed/", $path_to_vid),
                "path_to_vid"       => $path_to_vid,
                "path_to_webinar"   => $data['path_to_webinar'],
                "webinar"           => $data['webinar'],
            );
        }

        $status = UploadModule::find($id)->update($data_array);
        return response()->json($status);
    }

    public function delete($id){

        $result = UploadModule::where('module_id', $id)->first();

        $course = Module::with('course')
                    ->where('id', $result['module_id'])
                    ->first();

        $folder_name = "online_courses" . $this->sep . $course['course_id'] . $this->sep . $result['module_id'];
        Storage::deleteDirectory($folder_name);

        $status = $result->delete();

        $studentFileStatus =  FileDownloadStatus::where('module_id', $id)->delete();
        $status = $studentFileStatus;

        return response()->json($status);
    }

    public function download($id, $idx){

        // $result = UploadModule::where('module_id', $id)->first();
        // $course = Module::with('course')
        //             ->where('id', $result['module_id'])
        //             ->first();

        // $filenames = explode(";", $result['path_to_doc']);

        // $folder_name = storage_path("app"  . $this->sep . "online_courses" . $this->sep . $course['course_id'] . $this->sep) . $result['module_id'];
        // $zipFileName = str_replace(" ", "_", $course['name']) . '.zip';

        // $zip = new \ZipArchive();
        // $stat = $zip->open($folder_name . $this->sep . $zipFileName, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
        
        // foreach ($filenames as $file) {
        //     $zip->addFile($folder_name . $this->sep . $file, $file);
        // }

        // $zip->close();

        // return response()->download("$folder_name" . $this->sep . "$zipFileName");

        

        $result = UploadModule::where('module_id', $id)->first();
        $course = Module::with('course')
                    ->where('id', $result['module_id'])
                    ->first();

        $filename = explode(";", $result['path_to_doc'])[$idx];

        $folder_name = storage_path("app"  . $this->sep . "online_courses" . $this->sep . $course['course_id'] . $this->sep) . $result['module_id'];
        $full_path = $folder_name . $this->sep . $filename;

        return response()->download($full_path, $filename);

    }

    public function updateFileDownload($id, $idx){

        $file_download_array = array(
            "module_id" => $id,
            "user_id"   => Auth::user()->id,
            "file_idx"  => $idx,
        );

        $file_status = FileDownloadStatus::where('module_id', $id)
                    ->where('user_id', Auth::user()->id)
                    ->where('file_idx', $idx)
                    ->first();

        $status = "Ok";
    
        if($file_status){

        } else{
            $status = FileDownloadStatus::create($file_download_array);
        }

        return response()->json($status);

    }

}
