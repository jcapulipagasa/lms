<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FinalExam;
use App\FinalExamQuestion;
use App\StudentFinalExamProgress;
use App\StudentCourseEnroll;
use App\Module;
use App\StudentModuleProgress;
use App\ManageCourseCertificate;
use App\User;
use Auth;
use PDF;
use Storage;
use Mail;

class StudentFinalExamController extends Controller
{
    public function index(){

        $list = [];
        $enableButton = [];

        $course_ids = StudentCourseEnroll::select('courses')
                                        ->where('user_id', Auth::user()->id)
                                        ->first();

        $ids = explode(';', $course_ids['courses']);

        $course = FinalExam::with('course')
                            ->where('publish', 1)
                            ->get();

        foreach($course as $key => $val){
            if(in_array($val['course']['id'], $ids)){

                $module = Module::select('id')
                                ->where('course_id', $val['course']['id'])
                                ->orderBy('id', 'desc')
                                ->first();

                $data = StudentModuleProgress::select('modules')
                                        ->where('course_id', $val['course']['id'])
                                        ->where('user_id', Auth::user()->id)
                                        ->first();

                $modules = explode(';', $data['modules']);

                if(in_array($module['id'], $modules)){
                    $val['enabled'] = true;
                } else{
                    $val['enabled'] = false;
                }

                $list[] = $val;
                
            }
        }
        
        return $list;
    }

    public function getDetails($id){
        $course = FinalExam::with('course')
                            ->where('course_id', $id)
                            ->first();
        
        return $course;
    }

    public function checkIfPassed($id){

        $status = StudentFinalExamProgress::where('user_id', Auth::user()->id)
                                            ->where('course_id', $id)
                                            ->where('percentage', '>=', 80)
                                            ->first();

        if($status){
            return response()->json([
                'status' => 1,
                'message' => "You have already passed the final examination.",
            ], 200);            
        } else{

            $count = StudentFinalExamProgress::where('user_id', Auth::user()->id)
                                            ->count();
            if($count >= 2){

                $status = StudentFinalExamProgress::where('course_id', $id)
                                                ->orderBy('created_at', 'desc')
                                                ->first();

                return response()->json([
                    'status' => 1,
                    'message' => "You have taken the exam twice already.\n\nPlease check regularly the PSP FB page for announcements on re-opening of the course. Thank you.\n\n",
                ], 200);  

            } else{
                return response()->json([
                    'status' => 2
                ], 200);  
            }
           
        }

    }

    public function getExam($id){

        $module_ids = [];
        $randomize = [];
        $final_exam = [];

        $course = FinalExam::with('course')
                            ->where('course_id', $id)
                            ->first();

        $module = Module::select('id', 'randomized')
                            ->where('course_id', $id)
                            ->get();
    
        foreach($module as $val){
            $module_ids[] = $val['id'];
            $randomize[] = $val['randomized'];
        }

        foreach($module_ids as $key => $val){

            $final_exam[] = FinalExamQuestion::select('id', 'question', 'answer', 'correct_answer')
                        ->where('module_id', $val)
                        ->inRandomOrder()
                        ->limit($randomize[$key])
                        ->get()
                        ->toArray();
        }

        $final_exam_final = array_merge(...$final_exam);
    
        $data = self::formatFinalExamData($final_exam_final);

        return $data;

    }

    public function getResult(Request $request){

        $points = 0;

        $data = FinalExam::select('items')
                            ->where('course_id', $request['course'])
                            ->first();

        // $items = $data['items'];
        $items = count($request['answers']);
        $answers = $request['answers'];

        $questions = FinalExamQuestion::select('id', 'correct_answer')
                                    ->whereIn('id', $request['question_ids'])
                                    ->orderByRaw("field(id,".implode(',',$request['question_ids']).")")
                                    ->get();

        foreach($questions as $key => $val){
            if($val['correct_answer'] == $answers[$key]){
                $points = $points + 1;
            }
        }

        $computation = ($points / $items) * 100;
        $percentage = round($computation, 2);

        $status = StudentFinalExamProgress::create([
            'user_id' => Auth::user()->id,
            'course_id' => $request['course'],
            'score' => $points,
            'percentage' => $percentage
        ]);

        // if($percentage == 100){

        //     return response()->json([
        //         'status' => 1,
        //         'message' => "Congratulations! You passed the final examination.\n Please wait for your certificate in your registered email."
        //     ], 200);

        // } else
        
        if($percentage >= 80){

            // ----------------- Send Certificate of Completion ------------------------------- //

            $status = StudentFinalExamProgress::where('user_id', Auth::user()->id)
                                                ->where('course_id', $request['course'])
                                                ->where('percentage', '>=', 80)
                                                ->count();

            if($status == 1){
                // Prevent double sending of email

                if(Auth::user()->user_type != 'R'){
                    self::certificate($request['course']);
                } else{
                    self::certificateResident($request['course']);
                }

            }

            return response()->json([
                'status' => 1,
                'message' => "Congratulations! You passed the final examination.\n Please wait for your certificate in your registered email."
            ], 200);

        } else{

            return response()->json([
                'status' => 1,
                'message' => "\n\nYour score is $points/$items ($percentage%).\n\nYou need to retake the final examination.\nYou are allowed ONLY ONE RETAKE.\n\nPlease take time to review all learning materials."
            ], 200);

        }

    }

    private function formatFinalExamData($data){

        $quiz_id = [];
        $quiz_qna = [];

        foreach($data as $key => $val){

            $ans_arr = explode(";", $val['answer']);
            shuffle($ans_arr);

            // $quiz_id[] = $val['id'];
            $quiz_qna[] = array(
                "quiz_id"  => $val['id'],
                "question" => $val['question'],
                "answers"  => $ans_arr
            );
        }

        return compact('quiz_qna');
        // return compact('quiz_id', 'quiz_qna');

    }

    private function certificate($course_id){

        $user = User::with('profile')
                    ->where('id', Auth::user()->id)
                    ->first();

        $image_name = ManageCourseCertificate::where('course_id', $course_id)
                                            ->first();

        $full_name = $user['profile']['first_name'] . " " . $user['profile']['middle_name'] . " " . $user['profile']['last_name'];

        $serial_no = self::getSerialNumber($course_id);

        $pageLayout = array(190, 250);

        $pdf = new PDF();

        $pdf::AddPage('L', $pageLayout);

        // remove default header/footer
        $pdf::setPrintHeader(false);
        $pdf::setPrintFooter(false);

        // get the current page break margin
        $bMargin = $pdf::getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $pdf::getAutoPageBreak();
        // disable auto-page-break
        $pdf::SetAutoPageBreak(false, 0);
        // set bacground image

        $img_file = storage_path('app' . DIRECTORY_SEPARATOR .'course_certificate' . DIRECTORY_SEPARATOR . $course_id . DIRECTORY_SEPARATOR . $image_name['file_path']);
        $pdf::Image($img_file, 0, 0, 250, 190, 'JPG');

        // Name
        $html = '<p style="font-weight:700;font-size:18pt;text-align:center;">' . strtoupper("DR. " . $full_name) . '</p>';
        $pdf::writeHTMLCell(233, 0, 10, 73, $html);

        $html = '<p style="font-weight:700;font-size:10pt;text-align:center;">' . strtoupper("Serial No: $serial_no") . '</p>';
        $pdf::writeHTMLCell(60, 0, 195, 183, $html);

        $filename2 = Storage::getAdapter()->getPathPrefix() . 'cert_tmp' . DIRECTORY_SEPARATOR . Auth::user()->id . "_completion_" . $course_id . ".pdf";
        $pdf::Output($filename2, 'F');

        $pdf::Reset();

        $filename = self::reportCertificate($course_id, $serial_no);
        
        //Send email
        // self::mailToUser($filename, $user['email'], $full_name, $user['profile']['first_name']);
        self::mailToUser($filename, $user['email'], $full_name, $user['profile']['first_name'], $filename2);

        //Remove temporary files 
        $status = unlink(storage_path('app' . DIRECTORY_SEPARATOR . 'cert_tmp' . DIRECTORY_SEPARATOR . Auth::user()->id . "_completion_" . $course_id . ".pdf"));
        $status2 = unlink(storage_path('app' . DIRECTORY_SEPARATOR . 'final_cert_tmp' . DIRECTORY_SEPARATOR . "final_completion_dr_" . str_replace(" ", "_" , $full_name) . ".pdf"));

        return "Ok";

        // $status = Storage::delete('cert_temp' . DIRECTORY_SEPARATOR . $data['id'] . "_cert.pdf");
        // $status = Storage::delete($data['id'] . "_cert.pdf");
        // \Log::info($status);
    }

    private function certificateResident($course_id){

        $user = User::with('profile')
                    ->where('id', Auth::user()->id)
                    ->first();

        $image_name = ManageCourseCertificate::where('course_id', $course_id)
                                            ->first();

        $full_name = $user['profile']['first_name'] . " " . $user['profile']['middle_name'] . " " . $user['profile']['last_name'];

        $serial_no = self::getSerialNumber($course_id);

        $pageLayout = array(190, 250);

        $pdf = new PDF();

        $pdf::AddPage('L', $pageLayout);

        // remove default header/footer
        $pdf::setPrintHeader(false);
        $pdf::setPrintFooter(false);

        // get the current page break margin
        $bMargin = $pdf::getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $pdf::getAutoPageBreak();
        // disable auto-page-break
        $pdf::SetAutoPageBreak(false, 0);
        // set bacground image

        $img_file = storage_path('app' . DIRECTORY_SEPARATOR .'course_certificate' . DIRECTORY_SEPARATOR . $course_id . DIRECTORY_SEPARATOR . "Certificate_of_Completion_review.jpg");
        $pdf::Image($img_file, 0, 0, 250, 190, 'JPG');

        // Name
        $html = '<p style="font-weight:700;font-size:18pt;text-align:center;">' . strtoupper("DR. " . $full_name) . '</p>';
        $pdf::writeHTMLCell(233, 0, 10, 73, $html);

        $html = '<p style="font-weight:700;font-size:10pt;text-align:center;">' . strtoupper("Serial No: $serial_no") . '</p>';
        $pdf::writeHTMLCell(60, 0, 195, 183, $html);

        $filename2 = Storage::getAdapter()->getPathPrefix() . 'cert_tmp' . DIRECTORY_SEPARATOR . Auth::user()->id . "_completion_" . $course_id . ".pdf";
        $pdf::Output($filename2, 'F');

        // $pdf::Reset();

        // $filename = self::reportCertificate($course_id, $serial_no);
        
        //Send email
        // self::mailToUser($filename, $user['email'], $full_name, $user['profile']['first_name']);
        self::mailToResidentUser($filename2, $user['email'], $full_name, $user['profile']['first_name']);

        //Remove temporary files 
        $status = unlink(storage_path('app' . DIRECTORY_SEPARATOR . 'cert_tmp' . DIRECTORY_SEPARATOR . Auth::user()->id . "_completion_" . $course_id . ".pdf"));
        // $status2 = unlink(storage_path('app' . DIRECTORY_SEPARATOR . 'final_cert_tmp' . DIRECTORY_SEPARATOR . "final_completion_dr_" . str_replace(" ", "_" , $full_name) . ".pdf"));

        return "Ok";

        // $status = Storage::delete('cert_temp' . DIRECTORY_SEPARATOR . $data['id'] . "_cert.pdf");
        // $status = Storage::delete($data['id'] . "_cert.pdf");
        // \Log::info($status);
    }

    private function mailToUser($filename, $to_email, $to_name, $first_name, $filename2){

        $data = array( 'name' => $first_name );

        Mail::send('cert.report', $data, function($message) use ($to_name, $to_email, $filename, $filename2) {
            $message->to($to_email, $to_name);
            $message->subject('Certificate Completion Report');
            $message->from( config('mail.from.address') , 'admin@lms.app');
            $message->attach($filename);
            $message->attach($filename2);
        });

        if (Mail::failures()) {
            return false;
        }else{
            return true;
        }

    }

    private function mailToResidentUser($filename, $to_email, $to_name, $first_name){

        $data = array( 'name' => $first_name );

        Mail::send('cert.report', $data, function($message) use ($to_name, $to_email, $filename) {
            $message->to($to_email, $to_name);
            $message->subject('Certificate Completion Report');
            $message->from( config('mail.from.address') , 'admin@lms.app');
            $message->attach($filename);
        });

        if (Mail::failures()) {
            return false;
        }else{
            return true;
        }

    }

    public function reportCertificate($course_id, $serial_no){

        $info = User::with('profile')
                        ->where('id', Auth::user()->id)
                        ->first();

        $full_name = $info['profile']['first_name'] . " " . $info['profile']['middle_name'] . " " . $info['profile']['last_name'];

        $pageLayout = array(190, 250);

        $pdf = new PDF();

        $pdf::AddPage('P', $pageLayout);

        // remove default header/footer
        $pdf::setPrintHeader(false);
        $pdf::setPrintFooter(false);

        // get the current page break margin
        $bMargin = $pdf::getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $pdf::getAutoPageBreak();
        // disable auto-page-break
        $pdf::SetAutoPageBreak(false, 0);
        // set bacground image

        $img_file = storage_path('cert' . DIRECTORY_SEPARATOR . 'final_completion.jpg');

        $pdf::Image($img_file, 0, 0, 190, 250, 'JPG', '', '', false, 300, '', false, false, 0);
        // $pdf::Image($img_file, 0, 0, 250, 190, 'JPG');

        // Name
        // $html = '<p style="font-weight:700;font-size:20pt;color:#4c4c4c;text-align:center;">' . strtoupper("DR. " . $full_name) . '</p>';
        $html = '<p style="font-weight:bold;font-size:17pt;text-align:center;color:red;">' . strtoupper("DR. " . $full_name) . '</p>';
        $pdf::writeHTMLCell(193, 0, 0, 59, $html);

        $html = '<p style="font-weight:bold;font-size:16pt;text-align:center;">' . date("d F Y") . '</p>';
        $pdf::writeHTMLCell(233, 0, 16, 191, $html);

        $html = '<p style="font-weight:700;font-size:10pt;text-align:center;">' . strtoupper("Serial No: $serial_no") . '</p>';
        $pdf::writeHTMLCell(60, 0, 141, 244, $html);

        // $filename = Storage::getAdapter()->getPathPrefix() . 'final_cert_tmp' . DIRECTORY_SEPARATOR . ($key + 1) . "_final_completion_dr_" . str_replace(" ", "_" , $full_name) . ".pdf";
        $filename = Storage::getAdapter()->getPathPrefix() . 'final_cert_tmp' . DIRECTORY_SEPARATOR . "final_completion_dr_" . str_replace(" ", "_" , $full_name) . ".pdf";
        $pdf::Output($filename, 'F');

        return $filename;

    }

    // private function mailToUser($filename, $to_email, $to_name, $first_name){

    //     $data = array( 'name' => $first_name );

    //     Mail::send('cert.completion', $data, function($message) use ($to_name, $to_email, $filename) {
    //         $message->to($to_email, $to_name);
    //         $message->subject('Certificate of Completion');
    //         $message->from( config('mail.from.address') , 'admin@lms.app');
    //         $message->attach($filename);
    //     });

    //     if (Mail::failures()) {
    //         return false;
    //     }else{
    //         return true;
    //     }

    // }

    public function manualCertificate(){

        // $user = User::with('profile')
        //             ->where('id', 53)
        //             ->first();

        $user['id'] = 257;

        $course_id = 1;

        $image_name = ManageCourseCertificate::where('course_id', $course_id)
                                            ->first();

        // $full_name = $user['profile']['first_name'] . " " . $user['profile']['middle_name'] . " " . $user['profile']['last_name'];
        $full_name = "Francis G. Moria";

        // $data = array(
        //     'name' => ' Jose CARNATE Jr',
        //     'id'   => 2
        // );

        $pageLayout = array(190, 250);

        $pdf = new PDF();

        $pdf::AddPage('L', $pageLayout);

        // remove default header/footer
        $pdf::setPrintHeader(false);
        $pdf::setPrintFooter(false);

        // get the current page break margin
        $bMargin = $pdf::getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $pdf::getAutoPageBreak();
        // disable auto-page-break
        $pdf::SetAutoPageBreak(false, 0);
        // set bacground image

        // $img_file = storage_path('cert' . DIRECTORY_SEPARATOR . 'molecular_pathology.jpg');
        $img_file = storage_path('app' . DIRECTORY_SEPARATOR .'course_certificate' . DIRECTORY_SEPARATOR . $course_id . DIRECTORY_SEPARATOR . $image_name['file_path']);

        // $pdf::Image($img_file, 0, 0, 80, 105, 'JPG', '', '', false, 300, '', false, false, 0);
        $pdf::Image($img_file, 0, 0, 250, 190, 'JPG');

        // Name
        $html = '<p style="font-weight:700;font-size:20pt;color:#4c4c4c;text-align:center;">' . strtoupper("DR. " . $full_name) . '</p>';
        // $html = '<p style="font-weight:700;font-size:18pt;text-align:center;">' . strtoupper("DR. " . $data['name']) . '</p>';
        $pdf::writeHTMLCell(233, 0, 10, 73, $html);

        $html = '<p style="font-weight:700;font-size:10pt;text-align:center;">' . strtoupper("Serial No: " . self::getSerialNumber($course_id)) . '</p>';
        $pdf::writeHTMLCell(60, 0, 195, 183, $html);


        $filename = Storage::getAdapter()->getPathPrefix() . 'cert_tmp' . DIRECTORY_SEPARATOR . $user['id'] . "_completion_" . $course_id . ".pdf";
        // $filename = Storage::getAdapter()->getPathPrefix() . 'cert_tmp' . DIRECTORY_SEPARATOR . $data['name'] . "_completion_" . $course_id . ".pdf";
        $pdf::Output($filename, 'F');
        
        //Send email
        // self::mailToUser($filename, $user['email'], $full_name, $user['profile']['first_name']);

        //Remove temporary files 
        // $status = unlink(storage_path('app' . DIRECTORY_SEPARATOR . 'cert_tmp' . DIRECTORY_SEPARATOR . Auth::user()->id . "_completion_" . $course_id . ".pdf"));

        return "Ok";

        // $status = Storage::delete('cert_temp' . DIRECTORY_SEPARATOR . $data['id'] . "_cert.pdf");
        // $status = Storage::delete($data['id'] . "_cert.pdf");
        // \Log::info($status);
    }

    private function getSerialNumber($course_id){

        // return "20-0189";

        $count = StudentFinalExamProgress::where('percentage', '>=', 80)
                                            ->where('course_id', $course_id)
                                            ->count();

        $count = $count + 9;

        $serial = date('y') . "-" . sprintf("%04d", $count);

        return $serial;

    }


}
