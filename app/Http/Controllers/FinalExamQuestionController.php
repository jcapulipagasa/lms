<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FinalExamQuestion;

class FinalExamQuestionController extends Controller
{
    public function index($id){
        $questions = FinalExamQuestion::where('module_id', $id)
                                        ->get();

        return response()->json($questions);
    }

    public function save(Request $request){

        $data = $request->except('_token');

        $arr_data = array(
            'module_id'         => $data['module_id'],
            'question'          => $data['question'],
            'correct_answer'    => $data['correct_answer']
        );

        $ans = '';

        foreach($data['answer'] as $answer){

            if($ans){
                $ans = $ans . ";" . $answer;
            } else{
                $ans = $answer;
            }

        }
        
        $arr_data['answer'] = $ans;

        $status = FinalExamQuestion::create($arr_data);

        return response()->json($status);

    }

    public function show($id){

        $quiz = FinalExamQuestion::where('id', $id)
                ->first();
                
        $answers = explode(';', $quiz['answer']);

        $quiz['answer'] = $answers;

        return $quiz;

    }

    public function update(Request $request, $id){

        $data = $request->except('_token');

        $arr_data = array(
            'question'          => $data['question'],
            'correct_answer'    => $data['correct_answer']
        );

        $ans = '';

        foreach($data['answer'] as $answer){

            if($ans){
                $ans = $ans . ";" . $answer;
            } else{
                $ans = $answer;
            }

        }
        
        $arr_data['answer'] = $ans;

        $status = FinalExamQuestion::find($id)->update($arr_data);

        return response()->json($status);

    }

    public function delete($id){

        $status = FinalExamQuestion::destroy($id);

        if($status){
            return response()->json($status);
        }

        return response()->json($status);        
    }
}
