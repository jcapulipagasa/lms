<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ManageCourseCertificate;
use Storage;

class ManageCourseCertificateController extends Controller
{

    private $sep = DIRECTORY_SEPARATOR;

    public function index(){

        $data = ManageCourseCertificate::with('course')
                                        ->get();

        return $data;

    }

    public function save(Request $request){

        $data = $request->except('_token');

        $folder_name = 'course_certificate' . $this->sep . $data['course_id'];

        $path_name = "$folder_name" . $this->sep . $request->file('file')->getClientOriginalName();

        $request->file('file')->storeAs($folder_name, $request->file('file')->getClientOriginalName());

        $save_data = array(
            'course_id' => $data['course_id'],
            'file_path' => $request->file('file')->getClientOriginalName()
        );

        $status = ManageCourseCertificate::create($save_data);

        if($status){
            return response()->json($status);
        }

        return response()->json($status);

    }

    public function show($id){

        $data = ManageCourseCertificate::with('course')
                                ->where('id', $id)
                                ->first();

        return $data;

    }

    public function update(Request $request, $id){

        $status = '';

        $data = ManageCourseCertificate::where('id', $id)
                                ->first();

        if($request->file){

            $folder_name = 'course_certificate' . $this->sep . $data['course_id'];

            Storage::deleteDirectory($folder_name);

            $path_name = "$folder_name" . $this->sep . $request->file('file')->getClientOriginalName();
    
            $request->file('file')->storeAs($folder_name, $request->file('file')->getClientOriginalName());

        }

        $status = ManageCourseCertificate::find($id)->update(array("file_path" => $request->file('file')->getClientOriginalName()));
        return response()->json($status);

    }

    public function delete($id){

        $data = ManageCourseCertificate::where('id', $id)
                                ->first();

        $folder_name = 'course_certificate' . $this->sep . $data['course_id'];

        Storage::deleteDirectory($folder_name);

        $status = ManageCourseCertificate::destroy($id);

    }
}
