<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Module;

class ModuleController extends Controller
{
    public function index(){
        $course = Module::with('course')->get();
        return response()->json($course);
    }

    public function updateList(Request $request){
        $course = Module::with('course')
                        ->where('course_id', $request['course_id'])
                        ->get();
        return response()->json($course);
    }

    public function save(Request $request){
        $data = $request->except('_token');

        $status = Module::create($data);

        if($status){
            return response()->json($status);
        }

        return response()->json($status);

    }

    public function show($id){

        $course = Module::where('id', $id)
                    ->first();

        return response()->json($course);
    }

    public function update(Request $request, $id){

        $data = $request->except('_token');

        $status = Module::find($id)->update($data);

        return response()->json($status);
    }

    public function delete($id){

        $status = Module::destroy($id);

        if($status){
            return response()->json($status);
        }

        return response()->json($status);        
    }

    public function moduleList($id){

        $module_list = [];

        $data = Module::where('course_id', $id)
                ->get();

        foreach($data as $key => $value){
            $module_list[] = array(
                'text' => $value['name'],
                'value' => $value['id'],
            );
        }
        
        return $module_list;

    }

    public function getTitle($id){

        $course = Module::with('course')
                    ->where('id', $id)
                    ->first();

        return response()->json($course);
    }

}
