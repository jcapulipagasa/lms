<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Message;
use Auth;
use App\Events\NewMessage;

class ChatController extends Controller
{
    public function get(){

        $current_user_role = User::with('profile')
                            ->where("id", auth()->id())
                            ->first();

        if($current_user_role->profile->role == 'user'){
            $contacts = User::with('profile')
                            ->whereHas('profile', function($q) {
                                $q->where('role', 'admin');
                            })
                            ->first();

            return response()->json(array($contacts));
        }

        $contacts = User::with('profile')
            ->where('email_verified_at', "!=" , null)
            ->where("id", "!=", auth()->id())
            ->get();

        return response()->json($contacts);

    }

    public function getCurrentUser(){
        $user = User::with('profile')->where('email', Auth::user()->email)->first()->toArray();
        return response()->json($user);
    }

    public function getMessagesFor($id){

        $messages = Message::where(function($q) use ($id) {
            $q->where('from', auth()->id());
            $q->where('to', $id);
        })->orWhere(function($q) use ($id){
            $q->where('from', $id);
            $q->where('to', auth()->id());
        })->get();

        return response()->json($messages);
    }

    public function send(Request $request){
        $message = Message::create([
            'from' => Auth::user()->id,
            'to' => $request->contact_id,
            'message' => $request->text,
        ]);

        broadcast(new NewMessage($message));

        return response()->json($message);
    }
}
