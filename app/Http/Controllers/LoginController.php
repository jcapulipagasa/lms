<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class LoginController extends Controller
{
    public function login(Request $request){

        $data = $request->only('email', 'password');

        $exist = User::where('email', $data['email'])
                    ->exists();

        if($exist){

            $verified = User::select('email_verified_at')
                        ->where('email', $data['email'])
                        ->first();

            if($verified['email_verified_at']){

                if (Auth::attempt([                     // Login OK
                    'email' => $data['email'], 
                    'password' => $data['password'], 
                    'is_active' => 'Y'
                    ])) { 
        
                    return response()->json([
                        'message' => "OK",
                        'status' => 1
                    ], 200);
        
                } else{ //Wrong password
        
                    Auth::logout();
        
                    return response()->json([
                        'message' => "Invalid input or account is deactivated",
                        'status' => 0
                    ], 200);
                }

            } else{ //Unverified Email

                return response()->json([
                    'message' => "User is not yet verified by the Admin",
                    'status' => 0
                ], 200);

            }

        } else{ //Email does not exist

            return response()->json([
                'message' => "User is not registered",
                'status' => 0
            ], 200);

        }

    }

    public function checkAuth(){

        if(Auth::user()){
            return response()->json([
                'status' => 1
            ], 200);
        } else{
            return response()->json([
                'status' => 0
            ], 200);
        }

    }

    public function logout(){

        Auth::logout();
        return redirect('/');

    }

    public function getName(){
        
        $data = User::with('profile')
                        ->where('id', Auth::user()->id)
                        ->first();

        return $data['profile']['first_name'] . " " . $data['profile']['last_name'];

    }

}
