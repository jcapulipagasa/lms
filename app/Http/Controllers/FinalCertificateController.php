<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StudentFinalExamProgress;
use App\Profile;
use App\User;
use PDF;
use Storage;
use Mail;

class FinalCertificateController extends Controller
{

    public function certificate(){

        $course_id = 1;
        $serial_no = 78;
        $exam_id = 102;

        $data = StudentFinalExamProgress::where('percentage', '>=', 80)
                                        ->where('course_id', $course_id)
                                        // ->limit(1)
                                        ->where('id', $exam_id)
                                        ->get();
        
        foreach($data as $key => $val){

            $info = User::with('profile')
                            ->where('id', $val['user_id'])
                            ->first();

            $full_name = $info['profile']['first_name'] . " " . $info['profile']['last_name'];

            $pageLayout = array(190, 250);

            $pdf = new PDF();

            $pdf::AddPage('P', $pageLayout);

            // remove default header/footer
            $pdf::setPrintHeader(false);
            $pdf::setPrintFooter(false);

            // get the current page break margin
            $bMargin = $pdf::getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf::getAutoPageBreak();
            // disable auto-page-break
            $pdf::SetAutoPageBreak(false, 0);
            // set bacground image

            $img_file = storage_path('cert' . DIRECTORY_SEPARATOR . 'final_completion.jpg');

            $pdf::Image($img_file, 0, 0, 190, 250, 'JPG', '', '', false, 300, '', false, false, 0);
            // $pdf::Image($img_file, 0, 0, 250, 190, 'JPG');

            // Name
            // $html = '<p style="font-weight:700;font-size:20pt;color:#4c4c4c;text-align:center;">' . strtoupper("DR. " . $full_name) . '</p>';
            $html = '<p style="font-weight:bold;font-size:17pt;text-align:center;color:red;">' . strtoupper("DR. " . $full_name) . '</p>';
            $pdf::writeHTMLCell(193, 0, 0, 59, $html);

            $html = '<p style="font-weight:bold;font-size:16pt;text-align:center;">' . date("d F Y", strtotime($val['created_at'])) . '</p>';
            $pdf::writeHTMLCell(233, 0, 10, 191, $html);

            $html = '<p style="font-weight:700;font-size:10pt;text-align:center;">' . strtoupper("Serial No: " . self::getSerialNumber($serial_no)) . '</p>';
            $pdf::writeHTMLCell(60, 0, 141, 244, $html);


            // $filename = Storage::getAdapter()->getPathPrefix() . 'final_cert_tmp' . DIRECTORY_SEPARATOR . ($key + 1) . "_final_completion_dr_" . str_replace(" ", "_" , $full_name) . ".pdf";
            $filename = Storage::getAdapter()->getPathPrefix() . 'final_cert_tmp' . DIRECTORY_SEPARATOR . "final_completion_dr_" . str_replace(" ", "_" , $full_name) . ".pdf";
            $pdf::Output($filename, 'F');

            // $number_id = "26";
            // $filename2 = Storage::getAdapter()->getPathPrefix() . 'cert_tmp' . DIRECTORY_SEPARATOR . $number_id . "_completion_1.pdf";

            // $pdf::Reset();
            
            // //Send email
            // self::mailToUser($filename, $info['email'], $full_name, $info['profile']['first_name']);
            // self::mailToUser($filename, $info['email'], $full_name, $info['profile']['first_name'], $filename2);
            // self::mailToUser($filename, 'beavis12345123@gmail.com', $full_name, $info['profile']['first_name'], $filename2);

            //Remove temporary files 
            // $status = unlink(storage_path('app' . DIRECTORY_SEPARATOR . 'cert_tmp' . DIRECTORY_SEPARATOR . Auth::user()->id . "_completion_" . $course_id . ".pdf"));

            // return "Ok";

            // $status = Storage::delete('cert_temp' . DIRECTORY_SEPARATOR . $data['id'] . "_cert.pdf");
            // $status = Storage::delete($data['id'] . "_cert.pdf");
            // \Log::info($status);
                            
        }

        // $image_name = ManageCourseCertificate::where('course_id', $course_id)
        //                                     ->first();
        return "$full_name created.";

    }

    private function mailToUser($filename, $to_email, $to_name, $first_name){

        $data = array( 'name' => $first_name );

        Mail::send('cert.report', $data, function($message) use ($to_name, $to_email, $filename) {
            $message->to($to_email, $to_name);
            $message->subject('Certificate Completion Report');
            $message->from( config('mail.from.address') , 'admin@lms.app');
            $message->attach($filename);
        });

        if (Mail::failures()) {
            return false;
        }else{
            return true;
        }

    }

    private function getSerialNumber($key){

        $serial = date('y') . "-" . sprintf("%04d", $key);
        return $serial;

    }

    public function getSerial(){

        // $arr = [];
        $arr = '';

        $data = StudentFinalExamProgress::where('percentage', '>=', 80)
                                        ->where('course_id', 1)
                                        // ->limit(1)
                                        // ->where('id', $user_id)
                                        ->get();

        foreach($data as $key => $val){
            $arr = $arr . "<br>" . $val['user_id'] . ',' . self::getSerialNumber($key + 1);
        }

        return $arr;
    }

    public function manualCertificate(){

        $course_id = 1;
        $serial_no = 179;
        $exam_id = 265;

        $data = StudentFinalExamProgress::where('percentage', '>=', 80)
                                        ->where('course_id', $course_id)
                                        // ->limit(1)
                                        ->where('id', $exam_id)
                                        ->first();

        $val = $data;
        
        // foreach($data as $key => $val){

            $info = User::with('profile')
                            ->where('id', $val['user_id'])
                            ->first();

            $full_name = $info['profile']['first_name'] . " " . $info['profile']['last_name'];
            // $full_name = "Francis G. Moria";
            // $val['created_at'] = '2020-05-23';

            $pageLayout = array(190, 250);

            $pdf = new PDF();

            $pdf::AddPage('P', $pageLayout);

            // remove default header/footer
            $pdf::setPrintHeader(false);
            $pdf::setPrintFooter(false);

            // get the current page break margin
            $bMargin = $pdf::getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf::getAutoPageBreak();
            // disable auto-page-break
            $pdf::SetAutoPageBreak(false, 0);
            // set bacground image

            $img_file = storage_path('cert' . DIRECTORY_SEPARATOR . 'final_completion.jpg');

            $pdf::Image($img_file, 0, 0, 190, 250, 'JPG', '', '', false, 300, '', false, false, 0);
            // $pdf::Image($img_file, 0, 0, 250, 190, 'JPG');

            // Name
            // $html = '<p style="font-weight:700;font-size:20pt;color:#4c4c4c;text-align:center;">' . strtoupper("DR. " . $full_name) . '</p>';
            $html = '<p style="font-weight:bold;font-size:17pt;text-align:center;color:red;">' . strtoupper("DR. " . $full_name) . '</p>';
            $pdf::writeHTMLCell(193, 0, 0, 59, $html);

            $html = '<p style="font-weight:bold;font-size:16pt;text-align:center;">' . date("d F Y", strtotime($val['created_at'])) . '</p>';
            $pdf::writeHTMLCell(233, 0, 10, 191, $html);

            $html = '<p style="font-weight:700;font-size:10pt;text-align:center;">' . strtoupper("Serial No: " . self::getSerialNumber($serial_no)) . '</p>';
            $pdf::writeHTMLCell(60, 0, 141, 244, $html);


            // $filename = Storage::getAdapter()->getPathPrefix() . 'final_cert_tmp' . DIRECTORY_SEPARATOR . ($key + 1) . "_final_completion_dr_" . str_replace(" ", "_" , $full_name) . ".pdf";
            $filename = Storage::getAdapter()->getPathPrefix() . 'final_cert_tmp' . DIRECTORY_SEPARATOR . "final_completion_dr_" . str_replace(" ", "_" , $full_name) . ".pdf";
            $pdf::Output($filename, 'F');

            // $number_id = "26";
            // $filename2 = Storage::getAdapter()->getPathPrefix() . 'cert_tmp' . DIRECTORY_SEPARATOR . $number_id . "_completion_1.pdf";

            // $pdf::Reset();
            
            // //Send email
            self::mailToUser($filename, $info['email'], $full_name, $info['profile']['first_name']);
            // self::mailToUser($filename, $info['email'], $full_name, $info['profile']['first_name'], $filename2);
            // self::mailToUser($filename, 'beavis12345123@gmail.com', $full_name, $info['profile']['first_name'], $filename2);

            //Remove temporary files 
            // $status = unlink(storage_path('app' . DIRECTORY_SEPARATOR . 'cert_tmp' . DIRECTORY_SEPARATOR . Auth::user()->id . "_completion_" . $course_id . ".pdf"));

            // return "Ok";

            // $status = Storage::delete('cert_temp' . DIRECTORY_SEPARATOR . $data['id'] . "_cert.pdf");
            // $status = Storage::delete($data['id'] . "_cert.pdf");
            // \Log::info($status);
                            
        // }

        // $image_name = ManageCourseCertificate::where('course_id', $course_id)
        //                                     ->first();
        return "$full_name created.";

    }

}
