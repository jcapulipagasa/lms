<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lecture;
use App\Http\Helpers\ReformatFilename;
use Storage;
use App\User;
 
class LectureController extends Controller
{

    private $sep = DIRECTORY_SEPARATOR;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $role = User::with('profile')->where('id', auth()->id())->first()['profile']['role'];

        if($role == 'user'){
            $lecture = Lecture::where('publish', 1)->get();
            return $lecture;
        }

        $lecture = Lecture::all();
        return $lecture;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only('title', 'remarks', 'publish');

        $id = Lecture::create($data)->id;

        $folder_name = "lectures" . $this->sep . $id;

        $files = $request->file('file');
        $path_str = '';

        foreach($files as $file){

            $name = ReformatFilename::reformat($file->getClientOriginalName());

            if($path_str){
                $path_str = $path_str . ";" . $name;
            } else{
                $path_str = $name;
            }

            $path_name = $folder_name . $this->sep . $name;
            $file->storeAs($folder_name, $name);

        }

        $data['path'] = $path_str;
        $status = Lecture::find($id)->update($data);
        
        if($status){
            return response()->json('OK');
        }

        return response()->json("Error");

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = Lecture::where('id', $id)->first()->toArray();

        if($result['publish'] === 1){
            $result['publish'] = true;
        } else{
            $result['publish'] = false;
        }

        $filenames = explode(";", $result['path']);

        foreach($filenames as $file){
            $result['names'][] = $file;
        }

        // $result['path'] = basename($result['path']);
        return $result;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $data = $request->only('title', 'remarks', 'publish');
        $status = '';

        if($request->file){

            $lecture     = Lecture::where('id', $id)->first();
            $folder_name = "lectures" . $this->sep . $id;

            Storage::deleteDirectory($folder_name);

            $files = $request->file('file');
            $path_str = '';
    
            foreach($files as $file){
    
                $name = ReformatFilename::reformat($file->getClientOriginalName());
    
                if($path_str){
                    $path_str = $path_str . ";" . $name;
                } else{
                    $path_str = $name;
                }
    
                $file->storeAs($folder_name, $name);
    
            }
    
            $data['path'] = $path_str;

        }

        $status = Lecture::find($id)->update($data);
        return response()->json($status);
 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $result = Lecture::where('id', $id)->first();

        $folder_name = "lectures" . $this->sep . $id;
        Storage::deleteDirectory($folder_name);

        $status = Lecture::destroy($id);

        return response()->json($status);        
    }

    public function download($id){

        $result = Lecture::where('id', $id)->first();
        $filenames = explode(";", $result['path']);

        $folder_name = storage_path("app"  . $this->sep . "lectures"  . $this->sep) . $id;
        $zipFileName = str_replace(" ", "_", $result['title']) . '.zip';

        $zip = new \ZipArchive();
        $stat = $zip->open($folder_name . $this->sep . $zipFileName, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
        
        foreach ($filenames as $file) {
            $zip->addFile($folder_name . $this->sep . $file, $file);
        }

        $zip->close();

        return response()->download("$folder_name" . $this->sep . "$zipFileName");

    }

}
