<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\Module;
use App\UploadModule;
use App\Quiz;
use App\StudentModuleProgress;
use App\FileDownloadStatus;
use App\User;
use Auth;
use PDF;
use Storage;
use Mail;

class StudentProgressController extends Controller
{

    public function courseDisplay($id){

        $course = Course::where('id', $id)
                    ->first();
                    
        return $course;

    }

    public function moduleDisplay($id){

        $module = Module::where('course_id', $id)
                    ->orderBy('created_at', 'asc')
                    ->get();

        $progress = StudentModuleProgress::select('modules')
                    ->where('course_id', $id)
                    ->where('user_id', Auth::user()->id)
                    ->first();
        
        if($progress){

            $finished_modules = explode(";", $progress['modules']);
            $flag = true;

            foreach($module as $key => $val){

                if(in_array($val['id'], $finished_modules)){
                    $module[$key]['enable'] = 1;
                } else if(!in_array($val['id'], $finished_modules) && $flag == true){
                    $module[$key]['enable'] = 1;
                    $flag = false;
                } else{
                    $module[$key]['enable'] = 0;
                }

            }

        } else{

            foreach($module as $key => $val){

                if($key == 0){
                    $module[$key]['enable'] = 1;
                } else{
                    $module[$key]['enable'] = 0;
                }

                
            }
        }

        return $module;

    }

    public function moduleUploadDisplay($id){

        $file_idx = [];
        $file_data = [];

        $module = UploadModule::with('module')
                    ->where('module_id', $id)
                    ->first();
                    
        $module['path_to_doc']      = explode(';', $module['path_to_doc']);
        $module['path_to_vid']      = explode(',', $module['path_to_vid']);
        $module['external_link']    = explode(',', $module['external_link']);
        $module['path_to_webinar']  = json_decode($module['path_to_webinar'], 1);


        $file_status = FileDownloadStatus::select('file_idx')
                                        ->where('module_id', $id)
                                        ->where('user_id', Auth::user()->id)
                                        ->orderBY('file_idx', 'asc')
                                        ->get();

        if($file_status){
            foreach($file_status as $val){
                $file_idx[] = $val['file_idx'];
            }
        }

        if($file_idx){
            for($i = 0; $i < count($module['path_to_doc']); $i++){
                if(in_array($i, $file_idx)){
                    $file_data[$i] = true;
                } else{
                    $file_data[$i] = false;
                }
                
            }
        } else{
            for($i = 0; $i < count($module['path_to_doc']); $i++){
                $file_data[$i] = false;
            }
        }

        if(in_array(true, $file_data)){
            $module['enable_file'] = false;
        } else{
            $module['enable_file'] = true;
        }

        $module['file_status'] = $file_data;
       
        return $module;

    }

    public function getQuiz($id){

        $quiz = Quiz::where('module_id', $id)
                ->inRandomOrder()
                ->limit('3')
                ->get();

    
        $data = self::formatQuizData($quiz);

        return $data;

    }

    public function checkQuiz($cid, $mid){

        $progress = StudentModuleProgress::select('modules', 'quiz_results')
                    ->where('course_id', $cid)
                    ->where('user_id', Auth::user()->id)
                    ->first();

        $finished_modules = explode(";", $progress['modules']);
        $score = explode(";", $progress['quiz_results']);

        if(in_array($mid, $finished_modules)){
            $key = array_search($mid, $finished_modules);

            return response()->json([
                'status' => 1,
                'message' => "You have already taken the quiz.\n You're score is " . $score[$key] . " out of 3."
            ], 200);
            
        } else{
            return response()->json([
                'status' => 2,
            ], 200);            
        }

    }

    public function checkAnswer(Request $request){

        $message = "";
        $points = 0;
        $status = "failed";
        $total_points = 0;

        $data = $request->except('_token');

        $answer = Quiz::select('correct_answer')
                    ->where('id', $data['quiz_id'])
                    ->first();
        
        if($data['items'] == 2){

            if($answer['correct_answer'] === $data['answer']){
                $points = 1;
                $total_points = $data['score'] + $points;
                $message = "You are correct!\n\nYou've scored " . $total_points . " out of 3 items.\n\n";
            } else{
                $total_points = $data['score'];
                $message = "Correct Answer: " . $answer['correct_answer'] . "\n\nYou've scored " . $total_points . " out of 3 items.\n\n";
            }

            if($total_points == '3'){
                $message = $message . "Very Good!\nPlease proceed to the next module.\n\n\nIf this is the last module, You may take the final exam for this course.";
                $status = "passed";
            }else if($total_points == '2'){
                $message = $message . "You passed this module.\nYou may proceed to the next.\n\n\nIf this is the last module, You may take the final exam for this course.";
                $status = "passed";
            } else{
                $message = $message . "Needs imporovement.\nPlease review the module and take this quiz again.";
            }

        } else{

            if($answer['correct_answer'] === $data['answer']){
                $message = "You are correct!";
                $points = 1;                
            } else{
                $message = "Correct Answer: " . $answer['correct_answer'];
                $points = 0;                
            }
        }

        return compact("message", "points", "status");
        // return array("message" => "Correct Answer: " . $answer['correct_answer'], "points" => 0);
        
    }

    public function updateModuleProgress(Request $request){

        $data = $request->except('_token');

        $course = Module::with('course')
                    ->where('id', $data['module_id'])
                    ->first();

        $progress = StudentModuleProgress::where('course_id', $course['course_id'])
                    ->where('user_id', Auth::user()->id)
                    ->first();

        if($progress){

            $status = StudentModuleProgress::find($progress['id'])->update([
                "modules" => $progress['modules'] . ";" . $data['module_id'],
                "quiz_results" => $progress['quiz_results'] . ";" . $data['score']
            ]);

        } else{

            $status = StudentModuleProgress::create([
                "user_id" => Auth::user()->id,
                "course_id" => $course['course_id'],
                "modules" => $data['module_id'],
                "quiz_results" => $data['score']
            ]);

            if(Auth::user()->user_type != 'R'){
                self::certificate();
            }

        }
    
        return response()->json($status);

    }

    private function formatQuizData($data){

        $quiz_id = [];
        $quiz_qna = [];

        foreach($data as $key => $val){

            $ans_arr = explode(";", $val['answer']);
            shuffle($ans_arr);

            $quiz_id[] = $val['id'];
            $quiz_qna[] = array(
                "question" => $val['question'],
                "answers"  => $ans_arr
            );
        }

        return compact('quiz_id', 'quiz_qna');

    }

    // public function certificate(){
    private function certificate(){

        $user = User::with('profile')
                    ->where('id', Auth::user()->id)
                    ->first();

        $full_name = $user['profile']['first_name'] . " " . $user['profile']['middle_name'] . " " . $user['profile']['last_name'];

        $data = array(
            'name' => 'John Christopher Galang Apuli',
            'id'   => 2
        );

        $pageLayout = array(190, 250);

        $pdf = new PDF();

        $pdf::AddPage('P', $pageLayout);

        // remove default header/footer
        $pdf::setPrintHeader(false);
        $pdf::setPrintFooter(false);

        // get the current page break margin
        $bMargin = $pdf::getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $pdf::getAutoPageBreak();
        // disable auto-page-break
        $pdf::SetAutoPageBreak(false, 0);
        // set bacground image

        // $img_file = storage_path('cert' . DIRECTORY_SEPARATOR . 'molecular_pathology.jpg');
        $img_file = storage_path('cert' . DIRECTORY_SEPARATOR . 'certificate.jpg');

        // $pdf::Image($img_file, 0, 0, 80, 105, 'JPG', '', '', false, 300, '', false, false, 0);
        $pdf::Image($img_file, 0, 0, 190, 250, 'JPG');


        // Name
        // $html = '<p style="font-weight:700;font-size:20pt;color:#4c4c4c;text-align:center;">' . strtoupper("DR. " . $full_name) . '</p>';
        $html = '<p style="font-weight:700;font-size:16pt;text-align:center;">' . strtoupper("DR. " . $full_name) . '</p>';
        $pdf::writeHTMLCell(170, 0, 10, 90, $html);


        $filename = Storage::getAdapter()->getPathPrefix() . 'cert_tmp' . DIRECTORY_SEPARATOR . Auth::user()->id . "_cert.pdf";
        $pdf::Output($filename, 'F');
        
        //Send email
        self::mailToUser($filename, $user['email'], $full_name, $user['profile']['first_name']);

        //Remove temporary files 
        $status = unlink(storage_path('app' . DIRECTORY_SEPARATOR . 'cert_tmp' . DIRECTORY_SEPARATOR . Auth::user()->id . "_cert.pdf"));

        return "Ok";

        // $status = Storage::delete('cert_temp' . DIRECTORY_SEPARATOR . $data['id'] . "_cert.pdf");
        // $status = Storage::delete($data['id'] . "_cert.pdf");
        // \Log::info($status);
    }

    private function mailToUser($filename, $to_email, $to_name, $first_name){

        $data = array( 'name' => $first_name );

        Mail::send('cert.email', $data, function($message) use ($to_name, $to_email, $filename) {
            $message->to($to_email, $to_name);
            $message->subject('Certificate of Enrollment');
            $message->from( config('mail.from.address') , 'admin@lms.app');
            $message->attach($filename);
        });

        if (Mail::failures()) {
            return false;
        }else{
            return true;
        }

    }

    ////////// ***********************  FOR MANUAL CREATION OF CERTIFICATE OF ENROLLMENT   *************************************//////


    public function manualCertificate(){

        // $user = User::with('profile')
        //             ->where('id', Auth::user()->id)
        //             ->first();

        // $full_name = $user['profile']['first_name'] . " " . $user['profile']['middle_name'] . " " . $user['profile']['last_name'];

        $data = array(
            'name' => 'Charmaine S. TEMPLONUEVO-FLORES',
            'id'   => 2
        );

        $pageLayout = array(190, 250);

        $pdf = new PDF();

        $pdf::AddPage('P', $pageLayout);

        // remove default header/footer
        $pdf::setPrintHeader(false);
        $pdf::setPrintFooter(false);

        // get the current page break margin
        $bMargin = $pdf::getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $pdf::getAutoPageBreak();
        // disable auto-page-break
        $pdf::SetAutoPageBreak(false, 0);
        // set bacground image

        // $img_file = storage_path('cert' . DIRECTORY_SEPARATOR . 'molecular_pathology.jpg');
        $img_file = storage_path('cert' . DIRECTORY_SEPARATOR . 'certificate.jpg');

        // $pdf::Image($img_file, 0, 0, 80, 105, 'JPG', '', '', false, 300, '', false, false, 0);
        $pdf::Image($img_file, 0, 0, 190, 250, 'JPG');


        // Name
        // $html = '<p style="font-weight:700;font-size:20pt;color:#4c4c4c;text-align:center;">' . strtoupper("DR. " . $full_name) . '</p>';
        $html = '<p style="font-weight:700;font-size:16pt;text-align:center;">' . strtoupper("DR. " . $data['name']) . '</p>';
        $pdf::writeHTMLCell(170, 0, 10, 90, $html);


        $filename = Storage::getAdapter()->getPathPrefix() . 'cert_tmp' . DIRECTORY_SEPARATOR . $data['name'] . "_cert.pdf";
        $pdf::Output($filename, 'F');
        
        //Send email
        // self::mailToUser($filename, $user['email'], $full_name, $user['profile']['first_name']);

        //Remove temporary files 
        // $status = unlink(storage_path('app' . DIRECTORY_SEPARATOR . 'cert_tmp' . DIRECTORY_SEPARATOR . $data['name'] . "_cert.pdf"));

        return "Ok";

        // $status = Storage::delete('cert_temp' . DIRECTORY_SEPARATOR . $data['id'] . "_cert.pdf");
        // $status = Storage::delete($data['id'] . "_cert.pdf");
        // \Log::info($status);
    }    




}
