<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Course;
use App\StudentCourseEnroll;
use Auth;

class StudentEnrollCourseController extends Controller
{
    public function index(){
        $users = User::with('enroll')
                        ->with('profile')
                        ->where('email_verified_at', '<>', null)
                        ->where('is_active', 'Y')
                        ->whereHas('profile', function ($query) {
                            $query->where('role', '=', 'user');
                        })
                        ->orderBy('created_at', 'desc')
                        ->get();

        return $users;
    }

    public function studentCourseList(){

        $data = StudentCourseEnroll::select('courses')
                                    ->where('user_id', Auth::user()->id)
                                    ->first();

        $courses = explode(';', $data['courses']);

        $course_arr = Course::select('id', 'name')
                                ->whereIn('id', $courses)
                                ->get();

        return $course_arr;

    }

    public function save(Request $request){

        $courses = '';
        $user_id = $request['user_id'];
        $course_arr = $request['courses'];

        foreach($course_arr as $key => $val){
            if($courses){
                $courses = $courses . ';' . $val['value'];
            } else{
                $courses = $val['value'];
            }
        }

        $status = StudentCourseEnroll::create(compact('user_id', 'courses'));

        return response()->json($status);    

    }

    public function getEdit($id){

        $course_list = [];

        $data = StudentCourseEnroll::select('courses')
                                    ->where('user_id', $id)
                                    ->first();

        $courses = explode(';', $data['courses']);

        $course_arr = Course::select('id', 'name')
                                ->whereIn('id', $courses)
                                ->get();

        foreach($course_arr as $key => $value){
            $course_list[] = array(
                'text' => $value['name'],
                'value' => $value['id'],
            );
        }

        return $course_list;

    }

    public function update(Request $request){

        $courses = '';
        $user_id = $request['user_id'];
        $course_arr = $request['courses'];

        foreach($course_arr as $key => $val){
            if($courses){
                $courses = $courses . ';' . $val['value'];
            } else{
                $courses = $val['value'];
            }
        }

        $status = StudentCourseEnroll::where('user_id', $user_id)
                                    ->update(['courses' => $courses]);

        return response()->json($status);    

    }

}
