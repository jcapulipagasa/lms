<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FinalExamLog;
use Auth;
use DB;

class FinalExamLogController extends Controller
{
    public function pageHit($id){

        $log = FinalExamLog::where('course_id', $id)
                            ->where('user_id', Auth::user()->id)
                            ->first();

        if($log){

            FinalExamLog::where('course_id', $id)
                        ->where('user_id', Auth::user()->id)
                        ->update([
                            'exam_page_access'=> DB::raw('exam_page_access+1')
                        ]);

        } else{
            FinalExamLog::create([
                'course_id' => $id,
                'user_id' => Auth::user()->id,
                'exam_page_access' => 1
            ]);
        }

        return 1;

    }
}
