<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\StudentFinalExamProgress;
use App\Course;
use App\StudentCourseEnroll;
use App\StudentModuleProgress;
use App\Module;
use Auth;

class ViewGradesController extends Controller
{

    public function index(){
        $users = User::with('profile')
                        ->where('email_verified_at', '<>', null)
                        ->where('is_active', 'Y')
                        ->whereHas('profile', function ($query) {
                            $query->where('role', '=', 'user');
                        })
                        ->get();

        return $users;
    }

    public function finalExamStudentList(){

        $list = [];

        $users = User::with('profile')
                        ->where('email_verified_at', '<>', null)
                        ->where('is_active', 'Y')
                        ->whereHas('profile', function ($query) {
                            $query->where('role', '=', 'user');
                        })
                        ->get();

        foreach($users as $key => $val){
            $list[] = array(
                "value" => $val['id'],
                "text" => $val['profile']['first_name'] . " " . $val['profile']['middle_name'] . " " . $val['profile']['last_name']
            );
        }

        return $list;
    }

    public function updateFinalExamStudentList(Request $request){

        $list = [];

        $users = User::with('profile')
                        ->where('id', $request['id'])
                        ->get();
        
        return $users;
    }

    public function displayGrades($id){

        $list = StudentFinalExamProgress::with('course')
                                        ->where('user_id', $id)
                                        ->orderBy('created_at', 'desc')
                                        ->get();

        return $list;

    }

    public function dispalyGradesFilter(Request $request){


        $quiz_results = [];
        $final_results = [];

        $final = StudentFinalExamProgress::with('course')
                                        ->where('user_id', $request['id'])
                                        ->where('course_id', $request['course_id'])
                                        ->orderBy('created_at', 'asc')
                                        ->get()
                                        ->toArray();

        $quiz = StudentModuleProgress::where('user_id', $request['id'])
                            ->where('course_id', $request['course_id'])
                            // ->where('user_id', $request['id'])
                            ->first();


        if($final){

            $take_1 = '';
            $take_2 = 'N/A';

            if(count($final) == 2){

                if($final[1]['percentage'] >= 80){
                    $take_2 = 'Passed';
                } else{
                    $take_2 = 'Failed';
                }

                if($final[0]['percentage'] >= 80){
                    $take_1 = 'Passed';
                } else{
                    $take_1 = 'Failed';
                }
              
            } else{
                if($final[0]['percentage'] >= 80){
                    $take_1 = 'Passed';
                } else{
                    $take_1 = 'Failed';
                }                
            }

            $final_results[] = array(
                'name' => $final[0]['course']['name'],
                'take_1' => $take_1,
                'take_2' => $take_2
            );  

        }

        if($quiz){

            $module = explode(";", $quiz['modules']);

            $module_names = Module::whereIn('id', $module)
                                ->orderByRaw("field(id,".implode(',',$module).")")
                                ->get();

            foreach($module_names as $key => $val){
                $quiz_results[] = array(
                    'name' => $val['name'],
                    'score' => "Passed"
                );
            }

        }

    
        return compact('final_results', 'quiz_results');


        // $list = StudentFinalExamProgress::with('course')
        //                                 ->where('user_id', $request['id'])
        //                                 ->where('course_id', $request['course_id'])
        //                                 ->orderBy('created_at', 'desc')
        //                                 ->get();

        // return $list;

    }

    public function displayGradesStudent(){

        $grades = Course::with('final')
                    ->with('module')
                    ->whereHas('final', function ($query) {
                        $query->where('user_id', '=', Auth::user()->id);
                    })
                    ->whereHas('module', function ($query) {
                        $query->where('user_id', '=', Auth::user()->id);
                    })
                    ->get();

        $list = StudentFinalExamProgress::with('course')
                                        ->where('user_id', Auth::user()->id)
                                        ->orderBy('created_at', 'desc')
                                        ->get();

        return $list;

    }

    public function displayGradesStudentFilter(Request $request){

        $quiz_results = [];
        $final_results = [];

        $final = StudentFinalExamProgress::with('course')
                                        ->where('user_id', Auth::user()->id)
                                        ->where('course_id', $request['course_id'])
                                        ->orderBy('created_at', 'asc')
                                        ->get()
                                        ->toArray();

        $quiz = StudentModuleProgress::where('user_id', Auth::user()->id)
                            ->where('course_id', $request['course_id'])
                            // ->where('user_id', Auth::user()->id)
                            ->first();


        if($final){

            $take_1 = '';
            $take_2 = '';
            $status = '';

            if(count($final) == 2){

                if($final[1]['percentage'] >= 80){
                    $take_2 = 'Passed';
                    $status = 'Congratulations! Please check your certificate in your email. This may take a few days.';

                } else{
                    $take_2 = 'Failed';
                    $status = 'Please retake the course. Please check the PSP FB page for schedule of the next course opening';
                }

                if($final[0]['percentage'] >= 80){
                    $take_1 = 'Passed';
                } else{
                    $take_1 = 'Failed';
                }
              
            } else{
                if($final[0]['percentage'] >= 80){
                    $take_1 = 'Passed';
                    $take_2 = 'N/A';
                    $status = 'Congratulations! Please check your certificate in your email. This may take a few days.';

                } else{
                    $take_1 = 'Failed';
                    $status = 'You may take the examination one last time.';
                }                
            }

            $final_results[] = array(
                'name' => $final[0]['course']['name'],
                'take_1' => $take_1,
                'take_2' => $take_2
            );

        }

        if($quiz){

            $module = explode(";", $quiz['modules']);

            $module_names = Module::whereIn('id', $module)
                                ->orderByRaw("field(id,".implode(',',$module).")")
                                ->get();

            foreach($module_names as $key => $val){
                $quiz_results[] = array(
                    'name' => $val['name'],
                    'score' => "Passed"
                );
            }

        }

    
        return compact('final_results', 'quiz_results', 'status');

    }

    public function studentCourseList(){

        $course_list = [];

        // $data = StudentCourseEnroll::select('courses')
        //                             ->where('user_id', Auth::user()->id)
        //                             ->first();

        // $courses = explode(';', $data['courses']);

        $course_arr = Course::select('id', 'name')
                                // ->whereIn('id', $courses)
                                ->get();

        foreach($course_arr as $key => $value){
            $course_list[] = array(
                'text' => $value['name'],
                'value' => $value['id'],
            );
        }

        return $course_list;

    }

    public function adminCourseList($id){

        $course_list = [];

        $data = StudentCourseEnroll::select('courses')
                                    ->where('user_id', $id)
                                    ->first();

        $courses = explode(';', $data['courses']);

        $course_arr = Course::select('id', 'name')
                                ->whereIn('id', $courses)
                                ->get();

        foreach($course_arr as $key => $value){
            $course_list[] = array(
                'text' => $value['name'],
                'value' => $value['id'],
            );
        }

        return $course_list;

    }

    // public function formatGrades(){

    //     $arr = [];

    //     $h = fopen("D:\laragon\www\\tocsv\progress.csv", "r");
    
    //     $data = fgetcsv($h, 1000, ",");
    
    //     while (($data = fgetcsv($h, 1000, ",")) !== FALSE) 
    //         {

    //             $data_arr[] = $data;

    //             $grades = [];

    //             $idx    = explode(';', $data[1]);
    //             $scores = explode(';', $data[2]);

    //             foreach($idx as $key => $val){

    //                 if(!isset($grades[$val])){

    //                     $grades[$val] = $scores[$key];

    //                 }

    //             }

    //             $arr[] = array(

    //                 $data[3] . " " . $data[4],
    //                 $grades

    //             );

    //             // $arr[] = $data;
    //         }
    
    //     fclose($h);

    //     // return $arr;

    //     $for_csv = [];
    //     $for_csv[] = array();

    //     for($i = 0; $i <= count($arr); $i+=5){

    //         $concat_arr = [];

    //         // return $arr[$i];

    //         // ----------------------------------------------- Names ------------------------------------------//

    //         if(isset($arr[$i])){
    //             $concat_arr[0] = array('Name: ', $arr[$i][0], '', '');
    //             $concat_arr[1] = array('Module', 'Score', '', '');
    //         } else{
    //             $concat_arr[0] = array('', '', '', '');
    //             $concat_arr[1] = array('', '', '', '');
    //         }

    //         if(isset($arr[$i + 1])){
    //             array_push($concat_arr[0], 'Name: ', $arr[$i + 1][0], '', '');
    //             array_push($concat_arr[1], 'Module', 'Score', '', '');
    //         } else{
    //             array_push($concat_arr[0],'','', '', '');
    //             array_push($concat_arr[1],'','', '', '');
    //         }

    //         if(isset($arr[$i + 2])){
    //             array_push($concat_arr[0], 'Name: ', $arr[$i + 2][0], '', '');
    //             array_push($concat_arr[1], 'Module', 'Score', '', '');
    //         } else{
    //             array_push($concat_arr[0],'','', '', '');
    //             array_push($concat_arr[1],'','', '', '');
    //         }

    //         if(isset($arr[$i + 3])){
    //             array_push($concat_arr[0], 'Name: ', $arr[$i + 3][0], '', '');
    //             array_push($concat_arr[1], 'Module', 'Score', '', '');
    //         } else{
    //             array_push($concat_arr[0],'','', '', '');
    //             array_push($concat_arr[1],'','', '', '');
    //         }

    //         if(isset($arr[$i + 4])){
    //             array_push($concat_arr[0], 'Name: ', $arr[$i + 4][0], '', '');
    //             array_push($concat_arr[1], 'Module', 'Score', '', '');
    //         } else{
    //             array_push($concat_arr[0],'','', '', '');
    //             array_push($concat_arr[1],'','', '', '');
    //         }

    //         // $concat_arr[2] = array('','', '', '');

    //         // ----------------------------------------------- Scores ------------------------------------------//

    //         if(isset($arr[$i])){
    //             // $concat_arr[3] = array($arr[$i][1][], $arr[$i][1], '', '');
    //             $concat_arr[2] = array("1", isset($arr[$i][1][1]) ? $arr[$i][1][1] : "-", '', '');
    //             $concat_arr[3] = array("2", isset($arr[$i][1][2]) ? $arr[$i][1][2] : "-", '', '');
    //             $concat_arr[4] = array("3", isset($arr[$i][1][3]) ? $arr[$i][1][3] : "-", '', '');
    //             $concat_arr[5] = array("4", isset($arr[$i][1][4]) ? $arr[$i][1][4] : "-", '', '');
    //             $concat_arr[6] = array("5", isset($arr[$i][1][5]) ? $arr[$i][1][5] : "-", '', '');
    //             $concat_arr[7] = array("6", isset($arr[$i][1][6]) ? $arr[$i][1][6] : "-", '', '');
    //             $concat_arr[8] = array("7", isset($arr[$i][1][7]) ? $arr[$i][1][7] : "-", '', '');
    //             $concat_arr[9] = array("8", isset($arr[$i][1][8]) ? $arr[$i][1][8] : "-", '', '');
    //             $concat_arr[10] = array("9", isset($arr[$i][1][9]) ? $arr[$i][1][9] : "-", '', '');
    //             $concat_arr[11] = array("10", isset($arr[$i][1][10]) ? $arr[$i][1][10] : "-", '', '');
    //         } else{
    //             $concat_arr[2] = array('', '', '', '');
    //             $concat_arr[3] = array('', '', '', '');
    //             $concat_arr[4] = array('', '', '', '');
    //             $concat_arr[5] = array('', '', '', '');
    //             $concat_arr[6] = array('', '', '', '');
    //             $concat_arr[7] = array('', '', '', '');
    //             $concat_arr[8] = array('', '', '', '');
    //             $concat_arr[9] = array('', '', '', '');
    //             $concat_arr[10] = array('', '', '', '');
    //             $concat_arr[11] = array('', '', '', '');
    //         }

    //         if(isset($arr[$i + 1])){
    //             array_push($concat_arr[2], "1", isset($arr[$i + 1][1][1]) ? $arr[$i + 1][1][1] : "-", '', '');
    //             array_push($concat_arr[3], "2", isset($arr[$i + 1][1][2]) ? $arr[$i + 1][1][2] : "-", '', '');
    //             array_push($concat_arr[4], "3", isset($arr[$i + 1][1][3]) ? $arr[$i + 1][1][3] : "-", '', '');
    //             array_push($concat_arr[5], "4", isset($arr[$i + 1][1][4]) ? $arr[$i + 1][1][4] : "-", '', '');
    //             array_push($concat_arr[6], "5", isset($arr[$i + 1][1][5]) ? $arr[$i + 1][1][5] : "-", '', '');
    //             array_push($concat_arr[7], "6", isset($arr[$i + 1][1][6]) ? $arr[$i + 1][1][6] : "-", '', '');
    //             array_push($concat_arr[8], "7", isset($arr[$i + 1][1][7]) ? $arr[$i + 1][1][7] : "-", '', '');
    //             array_push($concat_arr[9], "8", isset($arr[$i + 1][1][8]) ? $arr[$i + 1][1][8] : "-", '', '');
    //             array_push($concat_arr[10], "9", isset($arr[$i + 1][1][9]) ? $arr[$i + 1][1][9] : "-", '', '');
    //             array_push($concat_arr[11], "10", isset($arr[$i + 1][1][10]) ? $arr[$i + 1][1][10] : "-", '', '');
    //         } else{
    //             array_push($concat_arr[2],'','', '', '');
    //             array_push($concat_arr[3],'','', '', '');
    //             array_push($concat_arr[4],'','', '', '');
    //             array_push($concat_arr[5],'','', '', '');
    //             array_push($concat_arr[6],'','', '', '');
    //             array_push($concat_arr[7],'','', '', '');
    //             array_push($concat_arr[8],'','', '', '');
    //             array_push($concat_arr[9],'','', '', '');
    //             array_push($concat_arr[10],'','', '', '');
    //             array_push($concat_arr[11],'','', '', '');
    //         }

    //         if(isset($arr[$i + 2])){
    //             array_push($concat_arr[2], "1", isset($arr[$i + 2][1][1]) ? $arr[$i + 2][1][1] : "-", '', '');
    //             array_push($concat_arr[3], "2", isset($arr[$i + 2][1][2]) ? $arr[$i + 2][1][2] : "-", '', '');
    //             array_push($concat_arr[4], "3", isset($arr[$i + 2][1][3]) ? $arr[$i + 2][1][3] : "-", '', '');
    //             array_push($concat_arr[5], "4", isset($arr[$i + 2][1][4]) ? $arr[$i + 2][1][4] : "-", '', '');
    //             array_push($concat_arr[6], "5", isset($arr[$i + 2][1][5]) ? $arr[$i + 2][1][5] : "-", '', '');
    //             array_push($concat_arr[7], "6", isset($arr[$i + 2][1][6]) ? $arr[$i + 2][1][6] : "-", '', '');
    //             array_push($concat_arr[8], "7", isset($arr[$i + 2][1][7]) ? $arr[$i + 2][1][7] : "-", '', '');
    //             array_push($concat_arr[9], "8", isset($arr[$i + 2][1][8]) ? $arr[$i + 2][1][8] : "-", '', '');
    //             array_push($concat_arr[10], "9", isset($arr[$i + 2][1][9]) ? $arr[$i + 2][1][9] : "-", '', '');
    //             array_push($concat_arr[11], "10", isset($arr[$i + 2][1][10]) ? $arr[$i + 2][1][10] : "-", '', '');
    //         } else{
    //             array_push($concat_arr[2],'','', '', '');
    //             array_push($concat_arr[3],'','', '', '');
    //             array_push($concat_arr[4],'','', '', '');
    //             array_push($concat_arr[5],'','', '', '');
    //             array_push($concat_arr[6],'','', '', '');
    //             array_push($concat_arr[7],'','', '', '');
    //             array_push($concat_arr[8],'','', '', '');
    //             array_push($concat_arr[9],'','', '', '');
    //             array_push($concat_arr[10],'','', '', '');
    //             array_push($concat_arr[11],'','', '', '');
    //         }

    //         if(isset($arr[$i + 3])){
    //             array_push($concat_arr[2], "1", isset($arr[$i + 3][1][1]) ? $arr[$i + 3][1][1] : "-", '', '');
    //             array_push($concat_arr[3], "2", isset($arr[$i + 3][1][2]) ? $arr[$i + 3][1][2] : "-", '', '');
    //             array_push($concat_arr[4], "3", isset($arr[$i + 3][1][3]) ? $arr[$i + 3][1][3] : "-", '', '');
    //             array_push($concat_arr[5], "4", isset($arr[$i + 3][1][4]) ? $arr[$i + 3][1][4] : "-", '', '');
    //             array_push($concat_arr[6], "5", isset($arr[$i + 3][1][5]) ? $arr[$i + 3][1][5] : "-", '', '');
    //             array_push($concat_arr[7], "6", isset($arr[$i + 3][1][6]) ? $arr[$i + 3][1][6] : "-", '', '');
    //             array_push($concat_arr[8], "7", isset($arr[$i + 3][1][7]) ? $arr[$i + 3][1][7] : "-", '', '');
    //             array_push($concat_arr[9], "8", isset($arr[$i + 3][1][8]) ? $arr[$i + 3][1][8] : "-", '', '');
    //             array_push($concat_arr[10], "9", isset($arr[$i + 3][1][9]) ? $arr[$i + 3][1][9] : "-", '', '');
    //             array_push($concat_arr[11], "10", isset($arr[$i + 3][1][10]) ? $arr[$i + 3][1][10] : "-", '', '');
    //         } else{
    //             array_push($concat_arr[2],'','', '', '');
    //             array_push($concat_arr[3],'','', '', '');
    //             array_push($concat_arr[4],'','', '', '');
    //             array_push($concat_arr[5],'','', '', '');
    //             array_push($concat_arr[6],'','', '', '');
    //             array_push($concat_arr[7],'','', '', '');
    //             array_push($concat_arr[8],'','', '', '');
    //             array_push($concat_arr[9],'','', '', '');
    //             array_push($concat_arr[10],'','', '', '');
    //             array_push($concat_arr[11],'','', '', '');
    //         }

    //         if(isset($arr[$i + 4])){
    //             array_push($concat_arr[2], "1", isset($arr[$i + 4][1][1]) ? $arr[$i + 4][1][1] : "-", '', '');
    //             array_push($concat_arr[3], "2", isset($arr[$i + 4][1][2]) ? $arr[$i + 4][1][2] : "-", '', '');
    //             array_push($concat_arr[4], "3", isset($arr[$i + 4][1][3]) ? $arr[$i + 4][1][3] : "-", '', '');
    //             array_push($concat_arr[5], "4", isset($arr[$i + 4][1][4]) ? $arr[$i + 4][1][4] : "-", '', '');
    //             array_push($concat_arr[6], "5", isset($arr[$i + 4][1][5]) ? $arr[$i + 4][1][5] : "-", '', '');
    //             array_push($concat_arr[7], "6", isset($arr[$i + 4][1][6]) ? $arr[$i + 4][1][6] : "-", '', '');
    //             array_push($concat_arr[8], "7", isset($arr[$i + 4][1][7]) ? $arr[$i + 4][1][7] : "-", '', '');
    //             array_push($concat_arr[9], "8", isset($arr[$i + 4][1][8]) ? $arr[$i + 4][1][8] : "-", '', '');
    //             array_push($concat_arr[10], "9", isset($arr[$i + 4][1][9]) ? $arr[$i + 4][1][9] : "-", '', '');
    //             array_push($concat_arr[11], "10", isset($arr[$i + 4][1][10]) ? $arr[$i + 4][1][10] : "-", '', '');
    //         } else{
    //             array_push($concat_arr[2],'','', '', '');
    //             array_push($concat_arr[3],'','', '', '');
    //             array_push($concat_arr[4],'','', '', '');
    //             array_push($concat_arr[5],'','', '', '');
    //             array_push($concat_arr[6],'','', '', '');
    //             array_push($concat_arr[7],'','', '', '');
    //             array_push($concat_arr[8],'','', '', '');
    //             array_push($concat_arr[9],'','', '', '');
    //             array_push($concat_arr[10],'','', '', '');
    //             array_push($concat_arr[11],'','', '', '');
    //         }

    //         $concat_arr[12] = array();
    //         $concat_arr[13] = array();

    //         array_push($for_csv, $concat_arr);

    //     }

    //     $fp = fopen('D:\laragon\www\\tocsv\result.csv', 'w');

    //     foreach ($for_csv as $fields) {

    //         foreach($fields as $val){
    //             fputcsv($fp, $val);
    //         }
            
    //     }

    //     fclose($fp);
    //     // return $for_csv;
    //     // return $arr;
    //     return 'OK';

    // }


}
