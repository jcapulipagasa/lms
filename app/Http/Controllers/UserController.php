<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Profile;
use Hash;
use Auth;
use Mail;

class UserController extends Controller
{
    public function index(){
        $user_list = User::orderBy('created_at', 'desc')->get();
        return $user_list;
    }
   
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(Request $request)
    {
        $data = $request->except('_token');

        $default_pass = "password";

        $email = filter_var($data['email'], FILTER_SANITIZE_EMAIL);

        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {

            $exist = User::where('email', $email)
                ->exists();

            if($exist){

                return response()->json([
                    'message' => "Error creating user. Email already exists.",
                    'status' => 0
                ], 200);

            } else{

                $created_user = User::create([
                    'email' => $data['email'],
                    'password' => Hash::make($default_pass),
                    'email_verified_at' => date('Y-m-d H:i:s'),
                    'is_active' => 'Y',
                    'user_type' => $data['user_type_selected'],
                ])->profile()->create([
                    'first_name' => $data['first_name'],
                    'middle_name' => $data['middle_name'],
                    'last_name' => strtoupper($data['last_name']),
                    'role' => $data['user_level'],
                    'contact_number' => $data['contact_num'],
                    'online_status' => 'N',
                ]);

                $user = User::with('profile')->find($created_user['user_id']);
                $mailed = self::mailToUser($user);

                return response()->json([
                    'message' => "User Registered successfully",
                    'status' => 1
                ], 200);
                
            }

        }

        return response()->json([
            'message' => "Error creating user. Please try again in a few minutes.",
            'status' => 0
        ], 200);

    }

    public function show(Request $request, $id){
        $user = User::with('profile')->where('id', $id)->first();
        return $user;
    }

    public function update(Request $request, $id){
        $data = $request->except('_token');
        
        $user = User::where('id', $id)->first();
        $user->email = $data['email'];
        $user->save();

        $profile = Profile::where('user_id', $id)->first();
        $profile->first_name = $data['first_name'];
        $profile->middle_name = $data['middle_name'];
        $profile->last_name = $data['last_name'];
        $profile->contact_number = $data['contact_num'];
        $profile->role = $data['user_level'];
        $profile->save();
        
        return "OK";
    }

    public function disableUser($id){
        $user = User::where('id', $id)->first();
        $user->is_active = 'N';
        $user->save();

        return "OK";
    }

    public function enableUser($id){
        $user = User::where('id', $id)->first();
        $user->is_active = 'Y';
        $user->save();

        return "OK";
    }

    public function register(Request $request){

        $data = $request->except('_token');

        $default_pass = "password";

        $email = filter_var($data['email'], FILTER_SANITIZE_EMAIL);

        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {

            $exist = User::where('email', $email)
                ->exists();

            if($exist){
                return response()->json([
                    'message' => "Error creating user. Email already exists.",
                    'status' => 0
                ], 200);
            } else{
                $created_user = User::create([
                    'email' => $data['email'],
                    'password' => Hash::make($default_pass),
                    'email_verified_at' => null,
                ])->profile()->create([
                    'first_name' => $data['first_name'],
                    'middle_name' => $data['middle_name'],
                    'last_name' => $data['last_name'],
                    'role' => 'user',
                    'contact_number' => $data['contact_num'],
                    'online_status' => 'N',
                ]);
            }

            if($created_user){

                return response()->json([
                    'message' => "User Registered. Please wait for an email from administrator upon verifying your account.",
                    'status' => 1
                ], 200);
            }

            return response()->json([
                'message' => "Error creating user. Please try again in a few minutes.",
                'status' => 0
            ], 200);


          } else {
            return response()->json([
                'message' => "Invalid email address format",
                'status' => 0
            ], 200);
          }

    }

    private function mailToUser($data){

        $to_name = $data->profile['last_name'] . ", " .  $data->profile['first_name'] . " " . $data->profile['middle_name'];
        $to_email = $data->email;

        $data = array( 'name' => $data->profile['first_name']);

        Mail::send('register.email', $data, function($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name)
                    ->subject('Invitation for Online Course');
            $message->from( config('mail.from.address') , 'admin@lms.app');
        });

        if (Mail::failures()) {
            return false;
        }else{
            return true;
        }

    }

}
