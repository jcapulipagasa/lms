<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;

class CourseController extends Controller
{
    public function index(){
        $course = Course::all();
        return response()->json($course);
    }

    public function save(Request $request){
        $data = $request->except('_token');

        $status = Course::create($data);

        if($status){
            return response()->json($status);
        }

        return response()->json($status);

    }

    public function show($id){

        $course = Course::where('id', $id)
                    ->first();

        return response()->json($course);
    }

    public function update(Request $request, $id){

        $data = $request->except('_token');

        // $status = Course::find($id);

        // \Log::info($data);

        $status = Course::find($id)->update($data);

        return response()->json($status);
    }

    public function delete($id){

        $status = Course::destroy($id);

        if($status){
            return response()->json($status);
        }

        return response()->json($status);        
    }

    public function courseList(){

        $course_list = [];

        $data = Course::all();

        foreach($data as $key => $value){
            $course_list[] = array(
                'text' => $value['name'],
                'value' => $value['id'],
            );
        }

        return $course_list;

    }

}
