<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Mail\MailNotify;
use Mail;


class ApproveRequestController extends Controller
{
    public function index(){
        return User::with('profile')->where('email_verified_at', null)->get(); 
    }

    public function approveRequest($id){

        $user = User::find($id);

        $user->email_verified_at = date('Y-m-d H:i:s');
        $user->is_active = 'Y';
        $status = $user->save();

        if($status){
            $user = User::with('profile')->find($id);
            $mailed = self::mailToUser($user);

            if($mailed){
                return $user;
            } else{
                return "error";
            }

        }

        return "error";
        
    }

    private function mailToUser($data){

        $to_name = $data->profile['last_name'] . ", " .  $data->profile['first_name'] . " " . $data->profile['middle_name'];
        $to_email = $data->email;

        $data = array( 'name' => $data->profile['first_name']);

        Mail::send('email.email', $data, function($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name)
                    ->subject('Verification Complete');
            $message->from( config('mail.from.address') , 'admin@lms.app');
        });

        if (Mail::failures()) {
            return false;
        }else{
            return true;
        }

    }
}
