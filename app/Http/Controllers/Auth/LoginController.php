<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;

use Redirect;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    // public function authenticated(Request $request, $user)
    // {
    //     if(!$user->email_verified_at) {
    //         Auth::logout();
    //         return Redirect::back()->withErrors(['User Not Verified', 'User not yet verified']);
    //     } 
    // }

    protected function authenticated(Request $request, $user)
    {
        if(!$user->is_active) {
            Auth::logout();

            return response()->json([
                'message' => "Invalid input",
                'status' => 0
            ], 200);
            
        } else {
            return response()->json([
                'message' => "OK",
                'status' => 1
            ], 200);
        }
    }

}
