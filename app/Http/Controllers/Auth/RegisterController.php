<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/registration-notification';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first-name' => ['required', 'string', 'max:255'],
            'middle-name' => ['required', 'string', 'max:255'],
            'last-name' => ['required', 'string', 'max:255'],
            'contact-number' => ['required', 'digits:11'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        // $user = new User;

        // $user->email                    = $data['email'];
        // $user->password                 = Hash::make($data['password']);
        // $user->profile->first_name      = $data['first-name'];
        // $user->profile->middle_name     = $data['middle-name'];
        // $user->profile->last_name       = $data['last-name'];
        // $user->profile->contact_number  = $data['contact-number'];
        // $user->profile->online_status   = "N";

        // $user->save();

        $created_user = User::create([
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'email_verified_at' => date('Y-m-d H:i:s'),
        ])->profile()->create([
            'first_name' => $data['first-name'],
            'middle_name' => $data['middle-name'],
            'last_name' => $data['last-name'],
            'contact_number' => $data['contact-number'],
            'online_status' => 'N',
            'role' => 'user',
        ]);

        return User::select('email', 'updated_at', 'created_at')
                ->where('email', $data['email'])
                ->first();
 
        // $created_user = User::create([
        //     'email' => $data['email'],
        //     'password' => Hash::make($data['password']),
        // ]);

        // \Log::info($created_user);
        // return $created_user;

    }
}
