<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Assignment;
use App\AssignmentList;
use App\User;
use Storage;

class AssignmentController extends Controller
{

    private $sep = DIRECTORY_SEPARATOR;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(){

        $assignment = Assignment::where('user_id', auth()->id())->get();

        $assignment_list = AssignmentList::all()->toArray();
        $assignment_list_arr = self::getAllAssignmentList($assignment_list);

        foreach($assignment as $key => $val){
            $assignment[$key]['title'] = $assignment_list_arr[$val['title_id']]['title'];
        }

        return $assignment;

    }

    public function indexAdmin($id){

        $assignment = Assignment::with('profile')
                        ->where('title_id', $id)
                        ->get()
                        ->map(function ($assignment){
                            return[
                                'id' => $assignment->id,
                                'name' => $assignment->profile->first_name . " " . $assignment->profile->last_name,
                                'review_uploaded' => $assignment->review_path,
                                'remarks' => $assignment->remarks
                            ];
                        });

        return $assignment;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only('title_id', 'remarks');
        $data['user_id'] = auth()->id();

        $folder_name = 'assignments' . $this->sep . $data['title_id'] . $this->sep . auth()->id();

        $path_name = "$folder_name" . $this->sep . $request->file('file')->getClientOriginalName();
        $request->file('file')->storeAs($folder_name, $request->file('file')->getClientOriginalName());

        $data['path'] = $path_name;

        $status = Assignment::create($data);

        if($status){
            return response()->json($status);
        }

        return response()->json($status);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = Assignment::where('id', $id)->first();
        $result['path'] = basename($result['path']);
        return $result;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->only('title_id', 'remarks');
        $status = '';

        if($request->file){

            $assignment     = Assignment::where('id', $id)->first();
            $path           = $assignment['path'];
            $folder_name = 'assignments' . $this->sep . $data['title_id'] . $this->sep . auth()->id();

            Storage::delete($path);

            $path_name = "$folder_name" . $this->sep . $request->file('file')->getClientOriginalName();
            $request->file('file')->storeAs($folder_name, $request->file('file')->getClientOriginalName());

            $data['path'] = $path_name;

        }

        $status = Assignment::find($id)->update($data);
        return response()->json($status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = Assignment::where('id', $id)->first();
        Storage::delete($result['path']);

        $status = Assignment::destroy($id);

        return response()->json($status);  
    }

    public function download($id){
        $result = Assignment::where('id', $id)->first();
        return response()->download(storage_path("app" . $this->sep . $result['path']));
    }

    public function uploadReview(Request $request, $id, $aid){

        $status = '';

        if($request->file){

            $assignment     = Assignment::where('id', $id)->first();
 
            $path           = $assignment['review_path'];
            $folder_name    = 'assignments'. $this->sep . $aid . $this->sep . $assignment['user_id'] . $this->sep . 'review';

            Storage::delete($path);

            $path_name = "$folder_name" . $this->sep . $request->file('file')->getClientOriginalName();
            $request->file('file')->storeAs($folder_name, $request->file('file')->getClientOriginalName());

            $data['review_path'] = $path_name;

            $status = Assignment::find($id)->update($data);
            return response()->json($status);

        }

        return response()->json("Error");
    }

    public function downloadReview($id){
        $result = Assignment::where('id', $id)->first();
        return response()->download(storage_path("app" . $this->sep . $result['review_path']));
    }


    private function getAllAssignmentList($data){

        $return = [];

        foreach ($data as $key => $val){
            $return[$val['id']] = $val;
        }

        return $return;

    }

}
