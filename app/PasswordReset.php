<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model
{
    protected $guarded = [];

    public $timestamps = false;

    protected $primaryKey = 'email';
    public $incrementing = false;
    
}
