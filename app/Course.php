<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $guarded = [];

    public function final(){
        return $this->hasMany(StudentFinalExamProgress::class);
    }

    public function module(){
        return $this->hasOne(StudentModuleProgress::class);
    }
}
