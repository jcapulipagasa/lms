<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ManageCourseCertificate extends Model
{

    protected $guarded = [];

    public function course(){
        return $this->belongsTo(Course::class);
    }
}
