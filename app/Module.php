<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $guarded = [];

    public function course(){
        return $this->belongsTo(Course::class);
    }

    public function upload(){
        return $this->hasOne(UploadModule::class);
    }

    public function final_exam(){
        return $this->hasMany(FinalExamQuestion::class);
    }

}
