<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
    protected $guarded = [];
    
    public function user(){
        $user =  $this->belongsTo(User::class);
        return $user;
    }

    public function profile(){
        return $this->hasOneThrough(Profile::class, User::class, 'id', 'user_id', 'user_id', 'id');
    }

    public function assignment_list(){
        return $this->hasOne(AssignmentList::class, 'id');
    }

}
