<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentModuleProgress extends Model
{
    protected $guarded = [];
}
