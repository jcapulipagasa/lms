<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FinalExamQuestion extends Model
{
    protected $guarded = [];
}
