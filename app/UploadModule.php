<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UploadModule extends Model
{
    protected $guarded = [];

    public function module(){
        return $this->belongsTo(Module::class);
    }
}
