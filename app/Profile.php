<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
        'user_id',
        'first_name', 
        'middle_name', 
        'last_name',
        'role',
        'contact_number',
        'online_status'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
