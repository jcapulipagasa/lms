<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileDownloadStatus extends Model
{
    protected $guarded = [];
}
