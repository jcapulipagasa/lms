// import Login from './components/Login'
// import LoginTest from './components/LoginForm'

import MainContent from './components/MainContent'

import Dashboard from './components/Dashboard'

import ApproveRequest from './components/ApproveRequest'

import Bulletin from './components/Bulletin'
import AddBulletin from './components/Bulletin/AddBulletin'
import EditBulletin from './components/Bulletin/EditBulletin'

import Lectures from './components/Lectures'
import AddLectures from './components/Lecture/AddLecture'
import EditLectures from './components/Lecture/EditLecture'
import LectureStudent from './components/Lecture/Student'

import AssignmentStudent from './components/Assignments/Assignments'
import AddAssignment from './components/Assignments/AddAssignment'
import EditAssignment from './components/Assignments/EditAssignment'
import ReviewAssignment from './components/Assignments/UploadReview'

import AssignmentAdmin from './components/Assignment'

import AboutPage from './components/About'
import AboutPageAdmin from './components/AboutPage/About'
import AddAbout from './components/AboutPage/AddAbout'
import EditAbout from './components/AboutPage/EditAbout'

import AssignmentList from './components/AssignmentsTitleList'
import AddAssignmentList from './components/AssignmentList/AddAssignmentList'
import EditAssignmentList from './components/AssignmentList/EditAssignmentList'

// CREATE USER
import UserList from './components/UserList'
import CreateNew from './components/UserList/CreateNew'
import UserDetails from './components/UserList/UserDetails'

//Enroll Student to Course
import EnrollStudentToCourse from './components/EnrollStudent/EnrollStudent.vue'
import AddEnrollStudentToCourse from './components/EnrollStudent/AddEnrollStudent.vue'
import EditEnrollStudentToCourse from './components/EnrollStudent/EditEnrollStudent.vue'

//ONLINE COURSES
//Courses
import CreateCourse from './components/OnlineCourses/Course/Course.vue'
import AddCourse from './components/OnlineCourses/Course/AddCourse.vue'
import EditCourse from './components/OnlineCourses/Course/EditCourse.vue'

//Module
import CreateModule from './components/OnlineCourses/Module/Module.vue'
import AddModule from './components/OnlineCourses/Module/AddModule.vue'
import EditModule from './components/OnlineCourses/Module/EditModule.vue'

//Upload Module
import UploadModule from './components/OnlineCourses/UploadModule/UploadModule.vue'
import AddUploadModule from './components/OnlineCourses/UploadModule/AddUploadModule.vue'
import EditUploadModule from './components/OnlineCourses/UploadModule/EditUploadModule.vue'

//Quiz
import Quiz from './components/OnlineCourses/Quiz/Quiz.vue'
import QuizList from './components/OnlineCourses/Quiz/QuizList.vue'
import AddQuiz from './components/OnlineCourses/Quiz/AddQuiz.vue'
import EditQuiz from './components/OnlineCourses/Quiz/EditQuiz.vue'

//Final Exam
import FinalExam from './components/OnlineCourses/FinalExam/FinalExam.vue'
import AddFinalExam from './components/OnlineCourses/FinalExam/AddFinalExam.vue'
import EditFinalExam from './components/OnlineCourses/FinalExam/EditFinalExam.vue'

//Final Exam Questions
import FinalExamQuestions from './components/OnlineCourses/FinalExamQuestionnaire/FinalExamQuestionnaire.vue'
import AddFinalExamQuestions from './components/OnlineCourses/FinalExamQuestionnaire/AddFinalExamQuestionnaire.vue'
import EditFinalExamQuestions from './components/OnlineCourses/FinalExamQuestionnaire/EditFinalExamQuestionnaire.vue'

//Final Exam Module
import FinalExamModule from './components/OnlineCourses/FinalExamModule/FinalExamModule.vue'
import EditRandomize from './components/OnlineCourses/FinalExamModule/EditRandomQuestion.vue'

//View Grades
import ShowGradeList from './components/OnlineCourses/ViewGrades/ViewGradesList.vue'
import ShowStudentGrade from './components/OnlineCourses/ViewGrades/ViewStudentGrade.vue'

//Manage Certificate
import ManageCertificate from './components/ManageCertificate/ManageCertificate.vue'
import AddManageCertificate from './components/ManageCertificate/AddManageCertificate.vue'
import EditManageCertificate from './components/ManageCertificate/EditManageCertificate.vue'

//Laboratory Manual
import StudentLaboratoryManualList from './components/LaboratoryManual/StudentList.vue'
import StudentAddLaboratoryManual from './components/LaboratoryManual/StudentAddLaboratoryManual.vue'

//-------------Student-------------------------//

//Courses
import StudentCourseList from './components/OnlineCourses/Student/Courses/CourseList.vue'
import StudentCourseMain from './components/OnlineCourses/Student/Courses/CourseMain.vue'
import StudentCourseModule from './components/OnlineCourses/Student/Courses/ModuleDisplay.vue'
import DisplayQuiz from './components/OnlineCourses/Student/Courses/DisplayQuiz.vue'
import DisplayFinalExam from './components/OnlineCourses/Student/Courses/DisplayFinalExam.vue'

import StudentFinalExamList from './components/OnlineCourses/Student/Courses/FinalExamList.vue'
import StudentFinalExamGrade from './components/OnlineCourses/Student/Courses/ViewStudentGrade.vue'


// import ViewAssignment from './components/Lecture/Student'

import Login from './components/Login/Login.vue'
import LoginBox from './components/Login/LoginBox.vue'
import Registration from './components/Login/RegistrationBox.vue'
import ResetPassword from './components/Login/ResetPassword.vue'
import ResetPasswordRequest from './components/Login/ResetPasswordRequest.vue'

import Chatbox from './components/Chatbox'


export const routes = [
 
    {
        path: '/',
        component: Login,
        children: [
            {
                name: 'login-box',
                path: '/',
                component: LoginBox
            },
            // {
            //     name: 'registration',
            //     path: '/registration',
            //     component: Registration
            // },
            {
                name: 'reset-password',
                path: '/password-reset/:code',
                component: ResetPassword
            },
            {
                name: 'reset-password-request',
                path: '/reset-password-request',
                component: ResetPasswordRequest
            },
        ]
    },
    {
        path: '/',
        component: MainContent,
        children: [
            {
                name: 'dashboard',
                path: '/dashboard',
                component: Dashboard
            },
            {
                name: 'dashboard-2',
                path: '/',
                component: Dashboard
            },
            {
                name: 'approve-request',
                path: '/approve-request',
                component: ApproveRequest
            },
            {
                name: 'bulletin',
                path: '/bulletin-main',
                component: Bulletin
            },
            {
                name: 'add-bulletin',
                path: '/add-bulletin',
                component: AddBulletin
            },
            {
                name: 'edit-bulletin',
                path: '/edit-bulletin/:id',
                component: EditBulletin
            },
            {
                name: 'lectures',
                path: '/lecture-admin',
                component: Lectures
            },
            {
                name: 'add-lecture',
                path: '/add-lecture',
                component: AddLectures
            },
            {
                name: 'edit-lecture',
                path: '/edit-lecture/:id',
                component: EditLectures
            },
            {
                name: 'student-lecture',
                path: '/student-lecture',
                component: LectureStudent
            },
            {
                name: 'add-assignment',
                path: '/add-assignment',
                component: AddAssignment
            },
            {
                name: 'edit-assignment',
                path: '/edit-assignment/:id',
                component: EditAssignment
            },
            {
                name: 'student-assignment',
                path: '/student-assignment',
                component: AssignmentStudent
            },
            {
                name: 'assignment-admin',
                path: '/assignment-admin/:id',
                component: AssignmentAdmin
            },
            {
                name: 'about-content-admin',
                path: '/about-content-admin',
                component: AboutPageAdmin
            },
            {
                name: 'add-about',
                path: '/add-about',
                component: AddAbout
            },
            {
                name: 'edit-about',
                path: '/edit-about/:id',
                component: EditAbout
            },
            {
                name: 'about-page',
                path: '/about-page',
                component: AboutPage
            },
            {
                name: 'chatbox',
                path: '/chatbox',
                component: Chatbox
            },
            {
                name: 'assignment-title',
                path: '/assignment-title',
                component: AssignmentList
            },
            {
                name: 'add-assignment-title',
                path: '/add-assignment-title',
                component: AddAssignmentList
            },
            {
                name: 'edit-assignment-title',
                path: '/edit-assignment-title/:id',
                component: EditAssignmentList
            },
            {
                name: 'upload-review',
                path: '/upload-review/:id/:aid',
                component: ReviewAssignment
            },

            {
                name: 'course-create',
                path: '/course-create',
                component: CreateCourse
            },
            {
                name: 'course-add',
                path: '/course-add',
                component: AddCourse
            },
            {
                name: 'course-edit',
                path: '/course-edit/:id',
                component: EditCourse
            },

            {
                name: 'module-create',
                path: '/module-create',
                component: CreateModule
            },
            {
                name: 'module-add',
                path: '/module-add',
                component: AddModule
            },
            {
                name: 'module-edit',
                path: '/module-edit/:id',
                component: EditModule
            },

            {
                name: 'upload-module-create',
                path: '/upload-module-create',
                component: UploadModule
            },
            {
                name: 'upload-module-add',
                path: '/upload-module-add/:id/:cid',
                component: AddUploadModule
            },
            {
                name: 'upload-module-edit',
                path: '/upload-module-edit/:id/:mid',
                component: EditUploadModule
            },

            {
                name: 'quiz-main',
                path: '/quiz-main',
                component: Quiz
            },
            {
                name: 'quiz-list',
                path: '/quiz-list/:id',
                component: QuizList
            },
            {
                name: 'add-quiz',
                path: '/add-quiz/:id',
                component: AddQuiz
            },
            {
                name: 'edit-quiz',
                path: '/edit-quiz/:id/:mid',
                component: EditQuiz
            },
            {
                name: 'student-course-list',
                path: '/student-course-list',
                component: StudentCourseList
            },
            {
                name: 'course-main',
                path: '/course-main/:id',
                component: StudentCourseMain
            },
            {
                name: 'course-module',
                path: '/course-module/:id/:cid',
                component: StudentCourseModule
            },
            {
                name: 'course-module-quiz',
                path: '/course-module-quiz/:id/:cid',
                component: DisplayQuiz
            },
            {
                name: 'user-list',
                path: '/user-list',
                component: UserList
            },
            {
                name: 'create-new-user',
                path: '/create-new-user',
                component: CreateNew
            },
            {
                name: 'get-user-details',
                path: '/get-user-details/:id',
                component: UserDetails
            },

            {
                name: 'final-exam-main',
                path: '/final-exam-main',
                component: FinalExam
            },
            {
                name: 'add-final-exam',
                path: '/add-final-exam',
                component: AddFinalExam
            },
            {
                name: 'edit-final-exam',
                path: '/edit-final-exam/:id',
                component: EditFinalExam
            },

            {
                name: 'final-exam-modules',
                path: '/final-exam-modules/:qid/:cid',
                component: FinalExamModule
            },
            {
                name: 'edit-final-randomize',
                path: '/edit-final-randomize/:mid/:qid/:cid',
                component: EditRandomize
            },

            {
                name: 'final-exam-questions',
                path: '/final-exam-questions/:id/:qid/:cid',
                component: FinalExamQuestions
            },
            {
                name: 'add-final-exam-question',
                path: '/add-final-exam-question/:id/:qid/:cid',
                component: AddFinalExamQuestions
            },
            {
                name: 'edit-final-exam-question',
                path: '/edit-final-exam-question/:id/:mid/:qid/:cid',
                component: EditFinalExamQuestions
            },

            {
                name: 'student-final-exam-list',
                path: '/student-final-exam-list',
                component: StudentFinalExamList
            },

            {
                name: 'course-final-exam',
                path: '/course-final-exam/:id',
                component: DisplayFinalExam
            },

            {
                name: 'view-grades-list',
                path: '/view-grades-list',
                component: ShowGradeList
            },
            {
                name: 'view-student-grades',
                path: '/view-student-grades/:id',
                component: ShowStudentGrade
            },

            {
                name: 'student-view-student-grades',
                path: '/student-view-student-grades',
                component: StudentFinalExamGrade
            },

            {
                name: 'enroll-student-to-course',
                path: '/enroll-student-to-course',
                component: EnrollStudentToCourse
            },
            {
                name: 'add-enroll-student-to-course',
                path: '/add-enroll-student-to-course/:id',
                component: AddEnrollStudentToCourse
            },
            {
                name: 'edit-enroll-student-to-course',
                path: '/edit-enroll-student-to-course/:id',
                component: EditEnrollStudentToCourse
            },

            {
                name: 'manage-course-certificate',
                path: '/manage-course-certificate',
                component: ManageCertificate
            },
            {
                name: 'add-manage-course-certificate',
                path: '/add-manage-course-certificate',
                component: AddManageCertificate
            },
            {
                name: 'edit-manage-course-certificate',
                path: '/edit-manage-course-certificate/:id',
                component: EditManageCertificate
            },

            {
                name: 'upload-laboratory-manual',
                path: '/upload-laboratory-manual',
                component: StudentLaboratoryManualList
            },
            {
                name: 'add-laboratory-manual',
                path: '/add-laboratory-manual',
                component: StudentAddLaboratoryManual
            },
            // {
            //     name: 'edit-manage-course-certificate',
            //     path: '/edit-manage-course-certificate/:id',
            //     component: EditManageCertificate
            // },
            
        ]
    }

]