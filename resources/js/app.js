/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Vue from 'vue'
import Router from 'vue-router';
import VueSidebarMenu from 'vue-sidebar-menu'
import 'vue-sidebar-menu/dist/vue-sidebar-menu.css'

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import VueFormWizard from 'vue-form-wizard'
import 'vue-form-wizard/dist/vue-form-wizard.min.css'

import Multiselect from 'vue-multiselect'

Vue.use(VueSidebarMenu)
Vue.use(Router);
Vue.use(VueFormWizard)
Vue.use(Multiselect)
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

Vue.component('multiselect', Multiselect)
Vue.component('lms-app', require('./components/LmsApp.vue').default);


// added for date format
Vue.use(require('vue-moment'));

import{ routes }from './routes.js'

const router = new Router({
    routes,
    mode: 'history'
});

const app = new Vue({
    el: '#app',
    router
});
