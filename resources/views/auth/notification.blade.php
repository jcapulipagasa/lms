@extends('layouts.app')

@section('content')
<div class="container container-margin">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Registration Complete') }}</div>

                <div class="card-body">

                    <div class="alert alert-success" role="alert">
                        {{ __('Please wait for an email regarding the verification of your registration by the admin.') }}
                        {{ __('If you did not receive any email, please contact administrator at ') }} {{config('mail.username')}}
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
