<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
</head>
<body>

    <div>

        <div style="font-size: large; font-weight: bold; border: 1px solid #d3d3d3; padding: 20px; background-color: #c4c4c4;">
            Verification Complete
        </div>

        <div style="border: 1px solid #d3d3d3; padding: 30px;">
            
            <p>
                Hi Dr. {{$name}},
            </p>
        
            <p>
                Your account has been verified by the admin. You may now login to the application using these credentials:
            </p>

            <p>
                Username: {your email address}<br>
                Password: password
            </p>

            <p>
                You can login to this link: {{ URL::to('/') }}<br>
                You can change the password through Forgot/Change Password in login page.
            </p>

            <p>
                BobongMD {{config('app.name')}}
            </p>

        </div>

    </div>

</body>
</html>




