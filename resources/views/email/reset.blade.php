<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
</head>
<body>

    <div>

        <div style="font-size: large; font-weight: bold; border: 1px solid #d3d3d3; padding: 20px; background-color: #c4c4c4;">
            Password Reset
        </div>

        <div style="border: 1px solid #d3d3d3; padding: 30px;">
            
            <p>
                Hi Dr. {{$name}},
            </p>
        
            <p>
                Here is your link to resetting your password.
            </p>

            <p>
                <a href="{{ URL::to('/password-reset') . '/' . $link  }}">{{ URL::to('/password-reset') . '/' . $link }}</a><br><br>
            </p>

            <p>
                If you did not request a password reset, please ignore this message.
            </p>

            <p>
                Thank you.
            </p>

            <p>
                BobongMD {{config('app.name')}}
            </p>

        </div>

    </div>

</body>
</html>




