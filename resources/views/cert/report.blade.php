<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
</head>
<body>

    <div>

        <div style="font-size: large; font-weight: bold; border: 1px solid #d3d3d3; padding: 20px; background-color: #c4c4c4;">
            Certificate Completion Report
        </div>

        <div style="border: 1px solid #d3d3d3; padding: 30px;">
            
            <p>
                Hi Dr. {{$name}},
            </p>
        
            <p>
                Attached is your certificate of completion report. Thank you for taking this course.
            </p>

            <p>
                BobongMD {{config('app.name')}}
            </p>

        </div>

    </div>

</body>
</html>