<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
</head>
<body>

    <div>

        <div style="font-size: large; font-weight: bold; border: 1px solid #d3d3d3; padding: 20px; background-color: #c4c4c4;">
            Invitation for Online Course
        </div>

        <div style="border: 1px solid #d3d3d3; padding: 30px;">
            
            <p>
                Dear Dr. {{$name}},
            </p>
        
            <p>
                This is to confirm your registration to LMS.BobongMD.com, a privately owned and developed learning management system. You will soon be enrolled to the PSP Online course. 
            </p>

            <p>
                To access the course please go to <a href="https://lms.bobongmd.com/">https://lms.bobongmd.com/</a> (copy and paste to the address bar of your browser). 
                Do not click “Register”, instead enter the following credentials:
            </p>

            <p>
                Username: {your email address}<br>
                Password: password
            </p>

            <p>
                Please change the default password on your next log in.
            </p>

            <p>
                After your successful log in, click the tab “Online Courses” on the left side of the screen.
                Then click “My Course list” which will show PSP Molecular Pathology.
            </p>

            <p>
                The button “Open the Course” will display the 10 topics labeled as “Modules” to facilitate the order of uploading and downloading the files. 
                Each module may consist of files to be downloaded, an external link for large-file downloading, a link to a video or a link to a webinar. 
                Read all the text documents, watch the video and attend the webinar if there is any for a particular module. 
            </p>

            <p>
                You may then take the self-assessment quiz – this button is not active unless you are done with all the learning materials in the module. 
                The next module will be available after taking the quiz of the previous module. 
                If you fail in the self-assessment quiz, you need to download one of the files and read it or watch the video in that module to re-activate the “Take the Quiz” button.
            </p>

            <p>
                The “Take final exam” button will be activated when you have taken the quiz of the tenth module.
            </p>

            <p>
                This course is better accessed using desktop or laptop.
            </p>

            <!-- <p>
                Our course enrollment is overloaded by more than 100%. Please bear with us if there are delays in downloading or slowing of the system and the server. 
                If you encounter problems, make sure to do your own trouble-shooting first by checking your internet speed or the computing power of your PC or gadget.
            </p> -->

            <p>
                For assistance we can only assist you by sending an email to MolePath.eCourse@gmail.com. 
                We cannot provide desktop support. For such problems, please consult IT.
            </p>

            <p>
                Please check your email regularly.
                Thank you.
            </p>

            <p>
                LMS.BobongMD.com
            </p>

        </div>

    </div>

</body>
</html>




